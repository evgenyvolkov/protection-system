#include "stdafx.h"
#include "3_3_28_15_AI.h"
#include "3_3_28_15_Robot.h"
#include "3_3_28_15_Violator.h"

void AI::remove(unsigned int x, unsigned int y)			//�������� ��������� � ��������� ������������
{
	for (vector<Platform*>::iterator it = platforms.begin(); it != platforms.end(); ++it)
	{
		if ((*it)->getX() == x && (*it)->getY())		//����� ���������
		{
			platforms.erase(it);
			return;
		}
	}
}

void AI::play(const Environment& E)						//�����, ���������� �� ����
{
	list <Cell*> all = getInfo(E);
	for each (Platform* p in platforms)					//��� ��������� ���� ����� � ���� � ����������				
		for each (Cell* c in all)
			if (typeid(*c) == typeid(Robot))
				((Platform*)p)->addSlave(c);
	for each (Platform* p in platforms)					//��� ��������� ���� ����� � ���� � ���������� ����� �������������� ������
		for each (Cell* c in all)
			if (typeid(*c) == typeid(Robot))
				((Platform*)p)->addSession(c);
	for (unsigned int i = 0; i < platforms.size(); i++)
		for each (Cell* c in all)
			if (typeid(*c) == typeid(Violator))
				platforms[i]->destroy(c);				//����������� �����������
	list <Robot*> allRobots = list<Robot*>();
	for each (Platform* p in platforms)					//��������� ������ �������
	{
		list <Cell*> robots = p->getRobots();
		for each (Cell* robot in robots)
			allRobots.push_back((Robot*)robot);
	}
	list <Violator*> violators = list<Violator*>();		//������ �����������
	for each (Cell* c in all)
		if (typeid(*c) == typeid(Violator))
			violators.push_back((Violator*)c);			//����������� �����������
	for each (Robot* robot in allRobots)				//������� ������ �������� ���� - ����������
	{
		Violator* v = findNear(robot, violators);		//����� ���������� �����������
		if (v)
			robot->setAim(v->getX(), v->getY());
		else
			robot->setAim(robot->getX() ? robot->getX()-1+rand()%3 : robot->getX() + rand()%2, robot->getY() ? robot->getY()-1+rand()%3 : robot->getY() + rand() %2);
		if (!robot->enable())
		{
			robot->setAim(robot->getX(), robot->getY());
		}
	}
	allRobots.sort();
	allRobots.unique();

}

list <Cell*> AI::getInfo(const Environment& E)			//��������� ���������� �� ��������� �� ���� ���������� ��������
{
	list <Cell*> totalList = list <Cell*>();
	for each (Platform* p in platforms)
	{
		list <Cell*> ls = p->getInfo(E);
		for each (Cell* c in ls)
			totalList.push_back(c);
	}	
	totalList.sort();									//���������� �� ���������
	totalList.unique();									//�������� ������������� �������
	return totalList;
}

Violator* AI::findNear(Robot* r, list<Violator*> violators)	//����� ���������� ���������� ��� ������� ������
{
	Violator* Temp;								
	if (violators.size() != 0)
	{
		Temp = *(violators.begin());
		for each(Violator* v in violators)
			if (dist(v, r) < dist(r, Temp))				//��������� ���������� �� ���������� ����������, ������������ �������� ������
				Temp = v;
		return Temp;
	}
	else
		return NULL;
}

unsigned int AI::dist(Cell* c1, Cell* c2)				//���������� ����� ����� ��������
{
	if (abs((int)(c1->getX() - c2->getX())) > abs((int)(c1->getY() - c2->getY())))
		return (unsigned int)abs((int)(c1->getX() - c2->getX()));
	return (unsigned int)abs((int)(c1->getY() - c2->getY()));

}
