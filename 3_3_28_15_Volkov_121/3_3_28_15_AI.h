#pragma once
#include <vector>
#include "3_3_28_15_Platform.h"
#include "3_3_28_15_Environment.h"
#include "3_3_28_15_Violator.h"
#include "3_3_28_15_Robot.h"


using namespace std;
//! ����� - ��(������������� ���������)

class AI
{
	vector <Platform*> platforms;														///<������; ������ ������ ��������, �������� ��������� ��		
public:
	AI() { platforms = vector <Platform*>(); }											///<������ �����������
	~AI() {}																			///<����������
	void add(Platform* p) { platforms.push_back(p); }									///<���������� ��������� ��������� \param p ��������� �� ���������
	void remove(unsigned int x, unsigned int y);										///<�������� ��������� \param x ���������� ������� param y ���������� �������
	void play(const Environment& env);													///<�������� ������� ������ \param env ������ �� ���������� �����
	list <Cell*> getInfo(const Environment& E);											///<��������� ���� ����������, ������� ����� �� \param E ������ �� ���������� ����� \return ������ ���������
	Violator* findNear(Robot* rob, list<Violator*> list);								///<����� ���������� ���������� ��� ������ ������ \param rob ��������� �� ������ \param list ������ ����������� \return ��������� �� ����������
	unsigned int dist(Cell* cell, Cell* cell_);											///< ������ ��������� ����� ����� ��������(����������) \param cell ��������� �� 1 ������ \param cell_ ��������� �� 2 ������ \return ����������
};

