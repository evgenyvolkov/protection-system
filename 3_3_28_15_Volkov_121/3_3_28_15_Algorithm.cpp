#include "stdafx.h"
#include "3_3_28_15_Algorithm.h"
#include "3_3_28_15_Platform.h"

void Algorithm::operator()(Environment& E, Violator& V, bool v)
{
	if(v)
		for (int i = 0; i < 10; i++)
		{
			unsigned int X = V.getX() - V.getSpeed() + rand() % (V.getSpeed() * 2 + 1);	//���������� ��������� ����������
			unsigned int Y = V.getY() - V.getSpeed() + rand() % (V.getSpeed() * 2 + 1);
			if (E.correct(X, Y) && !E.exist(X, Y))
			{

				V.setAimX(X);
				V.setAimY(Y);
				return;
			}
		}
	else
	{
		E.getInfo(V.getX(), V.getY(), 10);
		int maxX = 0;
		int maxY = 0;
		for (int i = 0; i < 100; i++)								//�������� ��������� ����������, ���� ��� ���������� - ��� ����
		{
			int x = V.getX() - V.getSpeed() + rand() % (2 * V.getSpeed() + 1);
			int y = V.getY() - V.getSpeed() + rand() % (2 * V.getSpeed() + 1);
			if (E.correct(x,y) && E.safe(x, y) && !E.exist(x,y))	//���� ���������� ���������, ���������� � �� ������ - ��1� ����
			{
				if (x > maxX)
				{
					maxX = x;
					maxY = y;
				}
				//return;
			}
		}
		V.setAimX(maxX);
		V.setAimY(maxY);
	}
}