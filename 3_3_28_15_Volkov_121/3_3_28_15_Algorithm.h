#pragma once

#include "3_3_28_15_Environment.h"
#include "3_3_28_15_Violator.h"

//! �����-������� �������� �������� ����������
class Algorithm
{
public:
	Algorithm() {}			///<������ �����������
	void operator()(Environment& E, Violator& P, bool v);	///<�������, ���� v = true, �� ��������� ��������, ����� - ��������� ������� \param E ������ �� ���������� ����� \param P ������ �� ���������� \param v ������ ����������, ���������� �� ��������� � ������ ���������
	~Algorithm() {}			///<����������
};
