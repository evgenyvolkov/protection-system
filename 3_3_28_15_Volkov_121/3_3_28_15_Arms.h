#pragma once
#include "3_3_28_15_Module.h"
#include "3_3_28_15_Cell.h"
#include <string>
#include <sstream>
using namespace std;
//! ����� - ����������, ����������� �� ������
class Arms :
	public Module
{
private:
	unsigned int time;						///<��������� ����� ��� �����������(����������� ���������� ����� ��������)
	unsigned int timeRecahrge;				///<���������� ����� �����������
public:
	Arms() : Module() { time = 1; }		///<������ �����������	
	Arms(unsigned int rad, unsigned int energyH, unsigned int energyL, unsigned int size, unsigned int t) : Module(rad, energyH, energyL, size) { time = t; timeRecahrge = 0; } ///<����������� \param rad ������ �������� \param energyH ������� ����������� ������� � �������� ������ \param energyL ������� ����������� ������� � ��������� ������ \param size ��������� ���������� ������ \param t ��������� ����� �����������

	~Arms() {} ///<����������
	virtual Module* clone() { return new Arms(*this); }		///<�������� ����� ������� \return ��������� �� ������ 
	virtual string toString() const;						///<��������� ������ ���������� \return ������
	bool canDestroy(unsigned int x, unsigned int y, Cell* c) { return timeRecahrge == 0 && abs((int)x - (int)c->getX()) <= radius && abs((int)y - (int)c->getY()) <= radius; }	///<�������� �� ����������� ����������� \param x ���������� ������� ������ \param y ���������� ������� ������ \param c ��������� �� ������ \return ������ ��������(1-��, 0-���)
	void destroy(unsigned int x, unsigned int y, Cell* c);	///<����������� ������ \param x ���������� ������� ������ \param y ���������� ������� ������ \param c ��������� �� ������
	void correctTime() { timeRecahrge = timeRecahrge > 0 ? timeRecahrge - 1 : 0; }	///<���������� ������� ����������� � ����� ����
	unsigned int getTime() const { return time; } ///<��������� ������� �����������
};

