#include "stdafx.h"
#include "3_3_28_15_Cell.h"



ostream& operator<< (ostream& s, const Cell& C)	//�������� ������
{

	s << C.toStr();
	return s;
}


std::string Cell::toStr() const
{
	ostringstream ost;
	ost << "Bar:      (" << x << ";" << y << ")\n";
	return std::string(ost.str().c_str());
}

unsigned int Cell::dist(Cell* c) const						//����� ��������� ���������� �� ������
{
	if (abs((int)c->getX() - (int)x) < abs((int)c->getY() - (int)y))
		return (unsigned int)abs((int)c->getY() - (int)y);
	return (unsigned int)abs((int)c->getX() - (int)x);
}


unsigned int Cell::dist(unsigned int X, unsigned int Y) const	//�� ���������
{
	if (abs((int)X - (int)x) < abs((int)Y - (int)y))
		return (unsigned int)abs((int)Y - (int)y);
	return (unsigned int)abs((int)X - (int)x);
}