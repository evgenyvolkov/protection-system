#pragma once
#include <string>
#include <sstream>

using namespace std;

//!������� ����� ����� - ������(����������� �� ���������), ������� ��� �������� � �����������
class Cell
{
protected:
	unsigned int x;												///<���������� �������
	unsigned int y;												///<���������� �������
	bool death;													///<������ ��������(1-���, 0-�����)
public:

	Cell(): Cell (0,0) {}								///<������ �����������
	Cell(unsigned int x, unsigned int y) : x(x), y(y) { death = false; }		///<����������� \param x ���������� ������� \param y ���������� �������
	~Cell() {}     ///<����������
	
	unsigned int getX() const { return x; }						///<��������� ���������� ������� \return ��������(int)
	unsigned int getY() const { return y; }						///<��������� ���������� ������� \return ��������(int)
	virtual Cell* clone() { return new Cell(*this); }			///<�������� ����� ������� \return ��������� �� ������
	virtual char print() { return 'B'; }						///<����� ������� �������(��� ����������� ����������) \return ��������(char)
	void dead() { death = true; }								///<������� ������ �������
	bool isDead() { return death; }								///<�������� �� ��, ��� ������ ��� ���� \return ��������(bool) 
	friend ostream& operator<< (ostream& s, const Cell& C);		///<������������� �������� ������ \param s ������ �� ����� \param C ������ �� ������(�������) \return �����
	virtual string toStr() const;							///<��������� ������ ���������� \return ������ 
	unsigned int dist(Cell* cell) const;								///<������ ���������� �� ������ ������ �� ������� \param cell ��������� �� ������ \return ����������
	unsigned int dist(unsigned int x, unsigned int y) const;	///<���������� �� ����������� ����� �� ����� \param x ���������� ������� \param y ���������� ������� \return ����������
	
};

