#include "stdafx.h"
#include "3_3_28_15_Environment.h"
#include "3_3_28_15_Platform.h"
#include "3_3_28_15_Robot.h"

Environment::Environment(unsigned int w, unsigned int h)		//������������, �����������
{
	if (w == 0)
		w = 1;
	if (h == 0)
		h = 1;
	width = w;
	height = h;
	field = list<Cell*>();
}

Environment::Environment(const Environment& E)
{
	width = E.width;
	height = E.height;
	field = list<Cell*>();
	for (list<Cell*>::const_iterator it = E.field.begin(); it != E.field.end(); ++it)
		field.push_back((*it)->clone());
}
Environment::Environment(Environment&& E)
{
	width = E.width;
	height = E.height;
	field = list<Cell*>();
	for (list<Cell*>::iterator it = E.field.begin(); it != E.field.end(); ++it)
	{
		field.push_back(*it);
		*it = NULL;
	}
}

Environment::~Environment()
{
	for (list<Cell*>::iterator it = field.begin(); it != field.end(); ++it)
		delete *it;
}

Environment& Environment::operator= (const Environment& E)
{
	if (this != &E)
	{
		width = E.width;
		height = E.height;
		field = list<Cell*>();
		for (list<Cell*>::const_iterator it = E.field.begin(); it != E.field.end(); ++it)
			field.push_back((*it)->clone());
	}
	return *this;
}

Environment& Environment::operator= (Environment&& E)
{
	if (this != &E)
	{
		width = E.width;
		height = E.height;
		field = list<Cell*>();
		for (list<Cell*>::iterator it = E.field.begin(); it != E.field.end(); ++it)
		{
			field.push_back(*it);
			*it = NULL;
		}
	}
	return *this;
}

ostream& operator<< (ostream& s, const Environment& E)	//�������� ������
{
	string* S = new string[E.height]();
	s << "   ";
	for (unsigned int i = 0; i < E.width; i++)			//����� ���������
		s << i / 10;
	s << "\n   ";
	for (unsigned int i = 0; i < E.width; i++)
		s << i % 10;
	s << endl;
	for (unsigned int i = 0; i < E.height; i++)
		S[i].resize(E.width, '-');
	for (list<Cell*>::const_iterator it = E.field.begin(); it != E.field.end(); ++it)	//����� ������� ������� � ��������������� ������ ������ ������
		S[(*it)->getY()][(*it)->getX()] = (*it)->print();
	for (unsigned int i = 0; i < E.height; i++)
	{
		string buf = "";		//���� ��������� �����
		buf += '0' + i / 10;
		buf += '0' + i % 10;
		buf += ' ';
		S[i] = buf + S[i];
	}
	for (unsigned int i = 0; i < E.height; i++)
		s << S[i] << endl;
	delete[] S;
	for (list<Cell*>::const_iterator it = E.field.begin(); it != E.field.end(); ++it)	//����� ����� � ����������� � ��������� ��������
		s << **it;
	return s;
}

void Environment::setHeight(unsigned int h)
{
	if (h >= height)
		height = h;
}

void Environment::setWidth(unsigned int w)
{
	if (w >= width)
		width = w;
}

Cell* Environment::getCell(unsigned int x, unsigned int y)
{
	return NULL;//�� ����������, ���� �� �����
}

Cell* Environment::add(Cell* c)
{
	if (find(c->getX(), c->getY()))							//���������� ������� �� �����
		throw exception("Place is not free");
	else
	{
		if(c->getX() >= width || c->getY() >= height)
			throw exception("Incorrect coordinates");
		field.push_back(c);
		return c;
	}
}

Cell* Environment::find(unsigned int x, unsigned int y)		//����� �� ����������
{
	for (list<Cell*>::iterator it = field.begin(); it != field.end(); ++it)
		if ((*it)->getX() == x && (*it)->getY() == y)
			return (*it);
	return NULL;
}

bool Environment::exist(unsigned int x, unsigned int y) const	//�������� �� �������������
{
	for (list<Cell*>::const_iterator it = field.begin(); it != field.end(); ++it)
		if ((*it)->getX() == x && (*it)->getY() == y)
			return true;
	return false;
}

void Environment::remove(unsigned int x, unsigned int y)
{
	for (list<Cell*>::iterator it = field.begin(); it != field.end(); ++it)	//��������
	{
		if ((*it)->getX() == x && (*it)->getY() == y)
		{
			delete *it;
			field.erase(it);
			return;
		}
	}
}

void Environment::correctPower()
{
	for each(Cell* c in field)
		if (typeid(*c) == typeid(Platform) || typeid(*c) == typeid(Robot))
			((Platform*)c)->correctPower();		//������������� ������������ �������� - �������� ������
}

list <Cell*> Environment::getInfo(unsigned int x, unsigned int y, unsigned int radius) const
{
	list <Cell*> ls = list<Cell*>();		//��������� ���� �������� ������ �������
	for each(Cell* c in field)
		if (abs((int)c->getX() - (int)x) <= (int)radius && abs((int)c->getY() - (int)y) <= (int)radius)
			ls.push_back(c);
	return ls;
}

void Environment::delCell(Cell* c)		//������� ������
{
	for (list<Cell*>::iterator it = field.begin(); it != field.end(); ++it)
		if (c == *it)
		{
			field.erase(it);
			return;
		}
}

void Environment::correctTime()			//������������ �����
{
	for each(Cell* c in field)
		if (typeid(*c) == typeid(Platform) || typeid(*c) == typeid(Robot))
			((Platform*)c)->correctTime();

}

list <Cell*> Environment::getInfoXY(unsigned int x, unsigned int y)
{
	Cell* c = find(x, y);							//��������� ����������, ������� ����� �������, ������������� �� ������ ������
	if (c)
		if (typeid(*c) == typeid(Platform) || typeid(*c) == typeid(Robot))
			return ((Platform*)c)->getInfo(*this);
	return list <Cell*>();

}

void Environment::moveRobot()					//������� �������
{
	for each (Cell* c in field)
		if (typeid(*c) == typeid(Robot) && ((Robot*)c)->enable())
			((Robot*)c)->move(*this);
}


bool Environment::safe(unsigned x, unsigned y)	//�������� �� �������������
{
	for each (Cell* c in field)
		if (typeid(*c) == typeid(Robot) || typeid(*c) == typeid(Platform))
			if (!((Platform*)c)->safe(x, y))
				return false;
	return true;
}

void Environment::correctNet()
{
	for each (Cell* c in field)
		if (typeid(*c) == typeid(Robot))
			((Robot*)c)->correctNet();
	for each (Cell* c in field)
		if (typeid(*c) == typeid(Robot) && !((Robot*)c)->enable())
			for each(Cell* z in field)
				if (typeid(*z) == typeid(Robot) || typeid(*z) == typeid(Platform))
					((Platform*)c)->remove(c);
}


bool Environment::netExist(unsigned int x, unsigned int y)
{
	for each (Cell* c in field)
		if (typeid(*c) == typeid(Robot) || typeid(*c) == typeid(Platform))
			if (((Platform*)c)->netExist(x, y))
				return true;
	return false;
}