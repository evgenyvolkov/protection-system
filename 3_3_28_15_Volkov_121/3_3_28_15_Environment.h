#pragma once
#include <list>
#include <string>
#include "3_3_28_15_Cell.h"


using namespace std;
//!����� - ���������� �����(���� ���������)
class Environment
{
private:
	list<Cell*> field;												///<������ �������� � �����
	unsigned int width;												///<������ ���� (�)
	unsigned int height;											///<������ ���� (�)
public:

	Environment() : Environment(1, 1) {} ///<������ �����������
	Environment(unsigned int w, unsigned int h); ///<����������� \param w ������ ���� \param h ������ ����
	Environment(const Environment& E); ///<���������� ����������� \param E ������ �� �����
	Environment(Environment&& E); ///<������������ ����������� \param E ������� ������ �� �����
	~Environment();		///<����������
	Environment& operator= (const Environment& E);					///<���������� �������� ������������ \param E ������ �� �����
	Environment& operator= (Environment&& E);							///<������������ �������� ������������ \param E ������� ������ �� ����� 
	friend ostream& operator<< (ostream& s, const Environment& E);	///<������������� �������� ������ \param s ������ �� ����� \param E ������ �� ����� \return �����



	unsigned int getWidth() const { return width; }					///<��������� ������ ���� \return ��������
	void setWidth(unsigned int x);									///<��������� ������ ���� \param x ������
	unsigned int getHeight() const { return height; }				///<��������� ������ ���� \return ��������
	void setHeight(unsigned int y);									///<��������� ������ ���� \param y ������
	Cell* getCell(unsigned int x, unsigned int y);					///<��������� ��������� �� ������ �� �������� ����������� \param x ���������� ������� ������ \param y ���������� ������� ������ \return ��������� �� ������
	Cell* add(Cell* cell);											///<���������� ������ ������� \param cell ��������� �� ������\return ��������� �� ������
	Cell* find(unsigned int x, unsigned int y);						///<����� ������� �� ����������� \param x ���������� ������� ������ \param y ���������� ������� ������ \return ��������� �� ������
	bool exist(unsigned int x, unsigned int y) const;				///<�������� ��������� ������ \param x ���������� ������� ������ \param y ���������� ������� ������ \return ������ ��������
	bool correct(unsigned int x, unsigned int y) const { return x < width && y < height; }	///<�������� ������������ ���� ��������� \param x ���������� ������� ������ \param y ���������� ������� ������ \return ������ ��������
	void remove(unsigned int x, unsigned int y);					///<�������� ������� �� �������� ����������� \param x ���������� ������� ������ \param y ���������� ������� ������
	void correctPower();											///<�������� ���� �������� � ��������������� ���������� �������������� 
	list <Cell*> getInfo(unsigned int x, unsigned int y, unsigned int radius) const;	///<��������� ���������� � �������� � ������� � �,� � �������� radius \param x ���������� ������� ������ \param y ���������� ������� ������ \param radius ������ \return ������ ��������
	void delCell(Cell* cell);											///<�������� �������� ������ � ���� \param cell ��������� �� ������
	void correctTime();												///<������������� ������� �� 1 ���
	list <Cell*> getInfoXY(unsigned int x, unsigned int y);			//<��������� ����������, ������� ����� �������, ������������� � ������ ������ \param x ���������� ������� ������ \param y ���������� ������� ������ \return ������ ��������
	void moveRobot();												///<�������� ����� �� 1 ���
	bool safe(unsigned x, unsigned y);								///<�������� ������������ ������ ������ ���� \param x ���������� ������� ������ \param y ���������� ������� ������ \return ������ ��������
	list<Cell*> getAll() { return field; } ///<��������� ���� \return ������ ��������(��� �����)
	void correctNet();  ///<������������� �����
	bool netExist(unsigned int x, unsigned int y); ///<�������� ������������� ����������� ���������� \param x ���������� ������� ������ \param y ���������� ������� ������ \return ������ ��������
};

