#include "stdafx.h"
#include "3_3_28_15_Game.h"
#include "3_3_28_15_Robot.h"
#include "3_3_28_15_Net.h"
#include "3_3_28_15_Sensor.h"
#include "3_3_28_15_Arms.h"
#include "3_3_28_15_Ray.h"
#include "3_3_28_15_Optic.h"
#include "3_3_28_15_Algorithm.h"
#include "3_3_28_15_Violator.h"

#include <fstream>

//Game::Game(char* file)
//{
//	env = Environment(30, 20);
//	ai = AI();
//	violators = list<Violator*>();
//	ifstream s;
//	s.open(file);
//	string type1 = "";
//	string type2 = "";
//	unsigned int x = 0;
//	unsigned int y = 0;
//	int levelHigh = 0;											//������� ����������� ��� ���������
//	int levelLow = 0;
//	int radius = 0;												//������ ��������
//	int size = 0;
//	int maxCount = 0;
//	int time = 0;
//	int power = 0;
//	int speed = 0;
//	string t = "";
//	while (!s.eof())
//	{
//		s >> type1;
//		if (type1 == "Module")
//		{
//			s >> type2;
//			s >> x;
//			s >> y;
//			s >> levelHigh;
//			s >> levelLow;
//			s >> radius;
//			s >> size;
//			try
//			{
//				if (type2 == "Net")
//				{
//					s >> maxCount;
//					addNet(x, y, radius, levelHigh, levelLow, size, maxCount);
//				}
//				else if (type2 == "Arms")
//				{
//					s >> time;
//					addArms(x, y, radius, levelHigh, levelLow, size, time);
//				}
//				else
//				{
//					s >> t;
//					addSensor(x, y, radius, levelHigh, levelLow, size, t == "Ray");
//				}
//			}
//			catch(...)
//			{
//
//			}
//		}
//		else
//		{
//			s >> type2;
//			s >> x;
//			s >> y;
//			try
//			{
//				if (type2 == "Robot")
//				{
//					s >> power;
//					s >> speed;
//					addRobot(x, y, power, speed);
//				}
//				else if (type2 == "Platform")
//				{
//					s >> power;
//					addPlatform(x, y, power);
//				}
//				else if (type2 == "Violator")
//				{
//					s >> speed;
//					addViolator(x, y, speed);
//				}
//				else
//					addBar(x, y);
//			}
//			catch (...)
//			{
//
//			}
//		}
//
//	}
//}

Game::Game(int width, int height)
{
	env = Environment(width, height);
	ai = AI();
	violators = list<Violator*>();
}


Game::~Game()
{
}


ostream& operator<< (ostream& s, const Game& G) 
{
	s << G.env;
	return s;
}

Cell* Game::addViolator(unsigned int x, unsigned int y, unsigned speed)
{
	try
	{
		Cell* c = env.add(new Violator(x, y, speed));	//������� ����������
		violators.push_back((Violator*)c);
		return c;
	}
	catch (exception e)
	{
		throw e;
	}
}

Cell* Game::addPlatform(unsigned int x, unsigned int y, unsigned int power)
{
	try
	{
		Cell* c = env.add(new Platform(x, y, power));  //�������� ���������
		ai.add((Platform*)c);
		return c;
	}
	catch (exception e)
	{
		throw e;
	}
}

Cell* Game::addRobot(unsigned int x, unsigned int y, unsigned int power, unsigned speed)
{
	try
	{
		Cell* c = env.add(new Robot(x, y, power, speed));		//�������� ������
		return c;
	}
	catch (exception e)
	{
		throw e;
	}
}

void Game::deleteCell(unsigned int x, unsigned int y)
{
	if (!env.find(x, y))
		throw exception("cell not found");
	else
	{
		ai.remove(x, y);
		for (list<Violator*>::iterator it = violators.begin(); it != violators.end(); ++it )
		{
			if ((*it)->getX() == x && (*it)->getY())		//������� ��������������� ������
				violators.erase(it);
		}
		env.remove(x, y);
	}
}//����� �� ������ ��� ������ ����
//Enter '0' to add Net, '1' - Sensor, '2' - Arm
void Game::addNet(unsigned int x, unsigned int y, unsigned int rad, unsigned int energyH, unsigned int energyL, unsigned int size, unsigned int count)
{
	Cell* c = env.find(x, y);
	if (!c)
		throw exception("cell not found");
	else
		if (typeid(*c) == typeid(Platform) || typeid(*c) == typeid(Robot))
			if (((Platform*)c)->getFreePlace() < size)
				throw exception("Platform is full");
			else
				((Platform*)c)->add(new Net(rad, energyH, energyL, size, count));
		else
			throw exception("Incorrect object");
}

void Game::addSensor(unsigned int x, unsigned int y, unsigned int rad, unsigned int energyH, unsigned int energyL, unsigned int size, bool type)
{
	Cell* c = env.find(x, y);
	if (!c)
		throw exception("cell not found");
	else
		if (typeid(*c) == typeid(Platform) || typeid(*c) == typeid(Robot))
			if (((Platform*)c)->getFreePlace() < size)
				throw exception("Platform is full");
			else
				if(type)
					((Platform*)c)->add(new Ray(rad, energyH, energyL, size));
				else
					((Platform*)c)->add(new Optic(rad, energyH, energyL, size));
		else
			throw exception("Incorrect object");
}

void Game::addArms(unsigned int x, unsigned int y, unsigned int rad, unsigned int energyH, unsigned int energyL, unsigned int size, unsigned int time)
{
	Cell* c = env.find(x, y);
	if (!c)
		throw exception("cell not found");
	else
		if (typeid(*c) == typeid(Platform) || typeid(*c) == typeid(Robot))
			if (((Platform*)c)->getFreePlace() < size)
				throw exception("Platform is full");
			else
				((Platform*)c)->add(new Arms(rad, energyH, energyL, size, time));
		else
			throw exception("Incorrect object");
}

void Game::deleteModule(unsigned int x, unsigned int y, unsigned int i)
{
	Cell* c = env.find(x, y);
	if (!c)
		throw exception("cell not found");
	else
		if (typeid(*c) == typeid(Platform) || typeid(*c) == typeid(Robot))
			if (i >= ((Platform*)c)->getCount())
				throw exception("Module not found");
			else
				((Platform*)c)->remove(i);
		else
			throw exception("Incorrect object");
}

void Game::play()				//���� ��� ����
{
	env.correctPower();			//������������ ��������
	env.correctTime();			//������������ �����
	ai.play(env);				//�� ������
	delViolator();				//������� ������ ������������
	env.moveRobot();			//����������� ���� �������
	stepViolator();				//��� �����������
	env.correctNet();
}

void Game::play(int x, int y)
{
	env.correctPower();			//������������ ��������
	env.correctTime();			//������������ �����
	ai.play(env);				//�� ������
	delViolator();				//������� ������ ������������
	env.moveRobot();			//����������� ���� �������
	for each(Violator* v in violators)
	{
		v->setAimX(x);
		v->setAimY(y);
	}
	for each(Violator* v in violators)
		v->move(env);			//������� ����������
	env.correctNet();
}

void Game::stepViolator()
{
	Algorithm A = Algorithm();
	for each(Violator* v in violators)
		A(env, *v,false);		//�������� �������, �������� ���� ����������
	for each(Violator* v in violators)
		v->move(env);			//������� ����������
}

void Game::delViolator()
{
	bool b = true;
	while (b)
	{
		b = false;
		for (list<Violator*>::iterator it = violators.begin(); !b && it != violators.end(); ++it)
			if ((*it)->isDead())
			{
				b = true;
				env.delCell(*it);
				delete *it;
				violators.erase(it);
				break;
			}
	}
}
