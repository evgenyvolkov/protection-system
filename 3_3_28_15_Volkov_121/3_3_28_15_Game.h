#pragma once

#include "3_3_28_15_Environment.h"
#include "3_3_28_15_AI.h"
#include "3_3_28_15_Violator.h"
#include "3_3_28_15_Bar.h"
#include <list>
#include <iterator>

//! ����� ����� ����
class Game																		
{
private:
	Environment env;																///<��������� ������ ���������� �����
	AI ai;																			///<��������� ������ ��
	list <Violator*> violators;														///<������ �����������

public:
	Game() :Game(0, 0) {}			///<������ �����������												
	Game(int width, int height);    ///<����������� \param w ������ ���� \param h ������ ����
	~Game();										///<����������

	unsigned int getFieldWidth() const { return env.getWidth(); }					///<��������� ������ ���� \return ��������
	unsigned int getFieldHeight() const { return env.getHeight(); }				///<��������� ������ ���� \return ��������
	void setFieldWidth(unsigned width) { env.setWidth(width); }					///<��������� ������ ���� \param x ������		
	void getFieldHeight(unsigned height) { env.setHeight(height);}				///<��������� ������ ���� \param y ������	
	friend ostream& operator<< (ostream& s, const Game& G);	//�������� ������	///<������������� �������� ������ \param s ������ �� ����� \param G ������ �� ���� \return �����

	Cell* addBar(unsigned int x, unsigned int y) { return env.add(new Bar(x, y)); }	///<���������� ����������� \param x ���������� ������� ������ \param y ���������� ������� ������  \return ��������� �� ������
	Cell* addViolator(unsigned int x, unsigned int y, unsigned speed);				///<���������� ����������  \param x ���������� ������� ������ \param y ���������� ������� ������ \param speed ��������  \return ��������� �� ������
	Cell* addPlatform(unsigned int x, unsigned int y, unsigned int power);						///<���������� ��������� \param x ���������� ������� ������ \param y ���������� ������� ������ \param ��������  \return ��������� �� ������
	Cell* addRobot(unsigned int x, unsigned int y, unsigned int power, unsigned speed);			///<���������� ������  \param x ���������� ������� ������ \param y ���������� ������� ������ \param �������� \param speed �������� \return ��������� �� ������
	void deleteCell(unsigned int x, unsigned int y);								///<�������� ������� \param x ���������� ������� ������ \param y ���������� ������� ������
	void addNet(unsigned int x, unsigned int y, unsigned int rad, unsigned int energyH, unsigned int energyL, unsigned int size, unsigned int count);	///<���������� ������ \param x ���������� ������� ������ \param y ���������� ������� ������ \param radius ������ \param energyH ������� ����������� ������� � �������� ������ \param energyL ������� ����������� ������� � ��������� ������ \param size ��������� ���������� ������ \count ����� �������������� �������
	void addSensor(unsigned int x, unsigned int y, unsigned int rad, unsigned int energyH, unsigned int energyL, unsigned int size, bool type);			///<���������� ������� \param x ���������� ������� ������ \param y ���������� ������� ������ \param radius ������ \param energyH ������� ����������� ������� � �������� ������ \param energyL ������� ����������� ������� � ��������� ������ \param type ���
	void addArms(unsigned int x, unsigned int y, unsigned int rad, unsigned int energyH, unsigned int energyL, unsigned int size, unsigned int time);	///<���������� ���������� \param x ���������� ������� ������ \param y ���������� ������� ������ \param radius ������ \param energyH ������� ����������� ������� � �������� ������ \param energyL ������� ����������� ������� � ��������� ������ \param size ��������� ���������� ������ \param time ����� �����������
	void deleteModule(unsigned int x, unsigned int y, unsigned int index);			///<�������� ������ \param x ���������� ������� ������ \param y ���������� ������� ������ \param index ��������������� ����������
	void play();																	///<�������� ������� ����
	void play(int x, int y);														///<������� ��� ���������� ������ \param x ���������� ������� ������ \param y ���������� ������� ������
	void stepViolator();															///<�������� �� ������ �����������(���������� ������ ����.���)
	void delViolator();																///<�������� ������ �����������
	list <Cell*> getInfoAI() {	return ai.getInfo(env); }							///<��������� ������ ��������, ������� ����� �� \return ������ ��������
	list <Cell*> getInfoXY(unsigned int x, unsigned int y) { return env.getInfoXY(x, y); };	///<��������� ���������� �� ����������  \return ������ ��������
	list <Cell*> getAll() { return env.getAll(); }                     ///<��������� ���� \return ������ ��������(��� �����)
	int getCount() const { return violators.size(); }				///<��������� ���������� ����������� \return ��������

};

