#pragma once

#include <string>
#include <sstream>

using namespace std;

//!����������� ������� ����� ��� ���� ������� - ������
class Module												
{
protected:
	int levelHigh;											///<������� ����������� ��� ��������� � �������� ������
	int levelLow;											///<������� ����������� ��� ��������� � ��������� ������
	int radius;												///<������ ��������
	int size;												///<���������� ����������� ������� ����� �� ���������
	bool enabled;											///<���/����

public:
	Module() : Module(1, 1, 0, 1) {} ///<������ �����������
	Module(unsigned int radius, unsigned int levelH, unsigned int levelL, unsigned int size) : levelHigh((levelH > levelL ? levelH : levelL)), levelLow((levelH > levelL ? levelL : levelH)), radius(radius), size(size), enabled(false) {} ///<����������� \param rad ������ �������� \param energyH ������� ����������� ������� � �������� ������ \param energyL ������� ����������� ������� � ��������� ������ \param size ��������� ���������� ������
	virtual Module* clone() = 0;         ///<�������� ����� ������� \return ��������� �� ������ 

	unsigned int getSize() const { return size; }			///<��������� ���������� ����������� ����� \return ��������
	void setSize(unsigned int s) { size = s; }				///<��������� ���������� ����������� ����� \param s ��������
	bool getEnabled() const { return enabled; }				///<��������� ���(����) \return �������� ������
	void setEnabled(bool e) { enabled = e; }				///<���������  ���(����)\param � �������� ������
	unsigned int getLevelHigh() const { return levelHigh; }; ///<��������� ������ ����������������� � �������� ������ \return ��������
	void setLevelHigh(unsigned int l) { levelHigh = l; }		///<��������� ������ ����������������� � �������� ������ \param l ��������
	unsigned int getLevelLow() const { return levelLow; };	///<��������� ������ ����������������� � ��������� ������ \return ��������
	void setLevelLow(int l) { levelLow = l; }						///<��������� ������ ����������������� � ��������� ������ \param l ��������
	virtual void off() { enabled = false; }					///<���������� ������
	void on() { enabled = true; }							///<���������� ������
	unsigned int tempLevel() const { return enabled ? levelHigh : levelLow; }	///<��������� �������� ������ ����������������� \return ��������
	unsigned int getRadius() const { return radius; }		///<��������� ������� �������� \return ��������

	friend ostream& operator<< (ostream& s, const Module& M);	///<������������� �������� ������ \param s ������ �� ����� \param C ������ �� ������ \return �����
	virtual string toString() const = 0;  ///<��������� ������ ���������� \return ������

	~Module() {} ///<����������
};

