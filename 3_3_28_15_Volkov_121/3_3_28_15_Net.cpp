#include "stdafx.h"
#include "3_3_28_15_Net.h"

#include "3_3_28_15_Robot.h"

string Net::toString() const
{
	ostringstream ost;
	ost << "Net:   " << " levelHigh = " << levelHigh << " levelLow = " << levelLow <<  " radius = " << radius << " size = " << size << " max count = " << maxCount;
	for (list<Robot*>::const_iterator it = robots.begin(); it != robots.end(); ++it)
	{
		ost << "\n   R: " << (*it)->getX() << " " << (*it)->getY();
	}
	for (list<Robot*>::const_iterator it = sessions.begin(); it != sessions.end(); ++it)
	{
		ost << "\n  *R: " << (*it)->getX() << " " << (*it)->getY();
	}
	return ost.str();
}

void Net::off()
{
	enabled = false;				//���������� 
	for each(Robot* r in robots)
		r->disconnect();
	robots.clear();
}

bool Net::add(unsigned int x, unsigned int y,Cell* C, Platform* p)		//�������� ����� � ������ �����
{
	if (enabled && !((Robot*)C)->enable() && abs(((int)((Robot*)C)->getX()) - (int)x) <= (int)radius && abs((int)((int)((Robot*)C)->getY()) - (int)y) <= (int)radius && (int)robots.size() < (int)maxCount)
	{
		((Robot*)C)->connect(p);
		robots.push_back(((Robot*)C));
		return true;
	}
	return false;	
}

list <Cell*> Net::getInfo(const Environment& E)
{												//��������� ����������
	list <Cell*> totalList = list <Cell*>();
	for each (Robot* m in robots)				//�� ���� ������� ��������
	{
		list <Cell*> ls = m->getInfo(E);
			for each (Cell* c in ls)
				totalList.push_back(c);
	}
	for each (Robot* m in sessions)				//�� ���� ������� �� �������
	{
		list <Cell*> ls = m->getInfo(E);
		for each (Cell* c in ls)
			totalList.push_back(c);
	}
	totalList.sort();							//��������� �� ��������
	totalList.unique();							//������� ������
	return totalList;
}

void Net::destroy(unsigned int x, unsigned int y, Cell* c)
{
	if (enabled)
	{
		for each (Robot* R in robots)			//������� ��������� �������� ������ ����� ��������
			R->destroy(c);
		for each (Robot* R in sessions)
			R->destroy(c);
	}
}

bool Net::addSession(unsigned int x, unsigned int y, Cell* c)	//��������� ������
{
	for each (Robot* r in robots)
	{
		if (r->addSlave(c))						//�������� � ������� ������ �������� ������������
		{
			sessions.push_back((Robot*)c);
			return true;
		}
	}
	return false;

}

list <Robot*> Net::getRobots()					//������������� �������
{
	list <Robot*> allRobots = list<Robot*>();
	for each (Robot* robot in robots)
		allRobots.push_back(robot);
	for each (Robot* robot in sessions)
		allRobots.push_back(robot);
	return allRobots;
}

bool Net::consist(Robot* R)						//�������� �� ������� ������
{
	for each (Robot* robot in robots)
		if (robot == R)
			return true;
	for each (Robot* robot in sessions)
		if (robot == R)
			return true;
	return false;
}

bool Net::willWorked(unsigned int x, unsigned int y)
{
	for each(Robot* robot in robots)
		if (robot->dist(x, y) > radius)			//�������� �������� 
			return false;
	return true;
}

void Net::correctNet(Robot* r, unsigned int x, unsigned int y)
{
	for (list<Robot*>::iterator it = robots.begin(); it != robots.end(); it++)
		if (*it == r && (*it)->dist(x, y) > radius)
			(*it)->disconnect();
	for (list<Robot*>::iterator it = sessions.begin(); it != sessions.end(); it++)
		if (*it == r && (*it)->dist(x, y) > radius)
			(*it)->disconnect();
}

void Net::remove(Cell* c)
{
	robots.remove((Robot*)c);
	//sessions.remove((Robot*)c);
}