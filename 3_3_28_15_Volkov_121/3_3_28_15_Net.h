#pragma once
#include "3_3_28_15_Module.h"
#include "3_3_28_15_Robot.h"
#include <list>

#include <string>
#include <sstream>
using namespace std;

//!����� - ������� ������, ����������� �� ������
class Net :												
	public Module
{
private:
	list <Robot*> robots;								///<�������(������) ����������
	list <Robot*> sessions;								///<������� �������(������), ��������� �� �������, ����� � �������� ���������� ����� ������ ����� �� ������ robots
	int maxCount;                                       ///<������������ ����� ������
public:
	Net() : Module() { robots = list<Robot*>(); sessions = list<Robot*>(); maxCount = 1; } ///<������ �����������
	Net(unsigned int rad, unsigned int energyH, unsigned int energyL, unsigned int size, unsigned int max) : Module(rad, energyH, energyL, size) { robots = list<Robot*>(); sessions = list<Robot*>(); maxCount = max; } ///<����������� \param rad ������ �������� \param energyH ������� ����������� ������� � �������� ������ \param energyL ������� ����������� ������� � ��������� ������ \param size ��������� ���������� ������ \param max ������������ ����� ������
	~Net() { off(); } ///<����������
	bool add(unsigned int x, unsigned int y, Cell* C, Platform* p);	///<������� �������� ������ (��� �������, ��� ��� ������ ����� � ��� ��������) \param x ���������� ������� ������ \param y ���������� ������� ������ \param C ��������� �� ������ \param P ��������� �� ���������
	virtual string toString() const;							///<��������� ������ ���������� \return ������
	virtual Module* clone() { return new Net(*this); }			///<�������� ����� ������� \return ��������� �� ������
	virtual void off();											///<������������� ������� ����������
	void destroy(unsigned int x, unsigned int y, Cell* c);		///<����������� ������ � ������� ����������� ������� \param x ���������� ������� ������ \param y ���������� ������� ������ \param c ��������� �� ������
	list <Cell*> getInfo(const Environment& E);					///<��������� ���������� � ����������� ������� \param E ������ �� ����� \return ������ ��������
	bool addSession(unsigned int x, unsigned int y, Cell* c);	///<���������� ������ ������������ \param x ���������� ������� ������ \param y ���������� ������� ������ \param c ��������� �� ������
	list <Robot*> getRobots();									///<��������� ������ ������� \return ������ �������
	bool consist(Robot* r);										///<�������� �� ������� ������ � ������ ����������� \param r ��������� �� ������ \return ������ ��������
	bool willWorked(unsigned int x, unsigned int y);			///<��������, �������� �� ���������� ������������ ��������, � ������ ����������� � ������ ����� \param x ���������� ������� ������ \param y ���������� ������� ������ \return ������ ��������
	void correctNet(Robot* r, unsigned int x, unsigned int y);	///<������������� ������ \param r ��������� �� ������ \param x ���������� ������� ������ \param y ���������� ������� ������
	void remove(Cell* c);    ///<�������� ������� ��� ������ ����� \param c ��������� �� ������
};

