#include "stdafx.h"
#include "3_3_28_15_Optic.h"
#include "3_3_28_15_Bar.h"
#include "3_3_28_15_Violator.h"

string Optic::toString() const
{
	ostringstream ost;
	ost << "Sensor:" << " levelHigh = " << levelHigh << " levelLow = " << levelLow << " radius = " << radius << " size = " << size << " type = optic";
	return ost.str();
}



list <Cell*> Optic::getInfo(unsigned int x, unsigned int y, const Environment& E)		//����� �������� ����������
{
	list <Cell*> ls = E.getInfo(x, y, radius);			//������� ���
	list <Cell*> del = list<Cell*>();
	for each(Cell* c in ls)
		if (typeid(*c) == typeid(Bar))					//������ ������� ��� ���������, ���� ����� ��� � ������� ����� �����������
		{
			for each (Cell* h in ls)
			{
				if ((h->getX() == x) && (h->getX() == c->getX()) && (Violator::sign((int)h->getY() - (int)c->getY()) != (Violator::sign((int)y - (int)c->getY()))))
					del.push_back(h);
				else
					if (h->getY() == y && h->getY() == c->getY() && Violator::sign((int)h->getX() - (int)c->getX()) != Violator::sign((int)x - (int)c->getX()))
						del.push_back(h);
					else
						if (Violator::sign((int)h->getX() - (int)c->getX()) != Violator::sign((int)x - (int)c->getX()) && Violator::sign((int)h->getY() - (int)c->getY()) != Violator::sign((int)y - (int)c->getY()))
							if (h->getY() != y && h->getY() != c->getY() && h->getX() != x && h->getX() != c->getX())
								if (
									abs(
										(double)((int)h->getX() - (int)c->getX())
										/
										((double)((int)h->getY() - (int)c->getY()))
										-
										((double)((int)x - (int)c->getX()))
										/
										((double)((int)y - (int)c->getY()))
										) < 0.2
									)
									del.push_back(h);
								else;
							else;
						else;
			}
		}
	for each(Cell* c in del)
		ls.remove(c);
	return ls;
}
