#pragma once
#include "3_3_28_15_Sensor.h"
//!����� - ���������� ������, ����������� �� �������(Sensor)
class Optic :											
	public Sensor
{
public:
	Optic() : Sensor() { } ///<������ �����������
	Optic(unsigned int rad, unsigned int energyH, unsigned int energyL, unsigned int size) : Sensor(rad, energyH, energyL, size) { } ///<����������� \param rad ������ �������� \param energyH ������� ����������� ������� � �������� ������ \param energyL ������� ����������� ������� � ��������� ������ \param size ��������� ���������� ������ 
	~Optic() {} ///<����������

	virtual Optic* clone() { return new Optic(*this); } ///<�������� ����� ������� \return ��������� �� ���������� ������
	virtual string toString() const;     ///<��������� ������ ���������� \return ������
	virtual list <Cell*> getInfo(unsigned int x, unsigned int y, const Environment& E);	///<������������� ������� ��������� ������ ���������� \param x ���������� ������� ������ \param y ���������� ������� ������ \param E ������ �� ����� \return ������ ����������
};

