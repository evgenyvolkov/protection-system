#include "stdafx.h"
#include "3_3_28_15_Platform.h"
#include "3_3_28_15_Net.h"
#include "3_3_28_15_Sensor.h"
#include "3_3_28_15_Optic.h"
#include "3_3_28_15_Ray.h"
#include "3_3_28_15_Robot.h"
#include "3_3_28_15_Violator.h"
#include "3_3_28_15_Arms.h"

Platform::Platform()
{
	modules = vector<Module*>();
	power = 1;
}


Platform::~Platform()
{
	for (unsigned int i = 0; i < modules.size(); i++)
		delete modules[i];
}

Platform::Platform(const Platform& P)									//���������� �����������
{
	power = P.power;
	modules = vector<Module*>();
	for (unsigned int i = 0; i < P.modules.size(); i++)
		modules.push_back(P.modules[i]->clone());
}
Platform::Platform(Platform&& P)									//������������ �����������
{
	power = P.power;
	modules = vector<Module*>();
	for (unsigned int i = 0; i < P.modules.size(); i++)
	{
		modules.push_back(P.modules[i]);
		P.modules[i] = NULL;
	}
}

Platform& Platform::operator= (const Platform& P)						//���������� �������� ������������
{
	if (this != &P)
	{
		power = P.power;
		modules = vector<Module*>();
		for (unsigned int i = 0; i < P.modules.size(); i++)
			modules.push_back(P.modules[i]->clone());
	}
	return *this;
}
Platform& Platform::operator= (Platform&& P)							//������������ �������� ������������
{
	if (this != &P)
	{
		power = P.power;
		modules = vector<Module*>();
		for (unsigned int i = 0; i < P.modules.size(); i++)
		{
			modules.push_back(P.modules[i]);
			P.modules[i] = NULL;
		}
	}
	return *this;
}

string Platform::toStr() const
{
	ostringstream ost;
	ost << "Platform: (" << x << ";" << y << ") Free place = " << getFreePlace() << ", power = " << power << endl;

	for (unsigned int i = 0; i < modules.size(); i++)
		ost << "M " << i << " : " << (modules[i]->getEnabled() ? " on " : "off ") << *modules[i] << endl;
	return ost.str();
}

unsigned int Platform::getFreePlace() const				//������������ ����� ��� �������� ������
{
	unsigned int place = SIZE;
	for (unsigned int i = 0; i < modules.size(); i++)
	{
		place -= modules[i]->getSize();
	}
	return place;
}

void Platform::remove(unsigned int i)
{
	vector<Module*>::iterator it = modules.begin() + i;
	Module* M = *it;
	modules.erase(it);
	delete M;
}

void Platform::correctPower()								//������������� �����������������
{
	for (unsigned int i = 0; i < modules.size(); i++)
		if (totalPower() > power)
			modules[i]->off();
	for each(Module* m in modules)
		if (totalPower() + m->getLevelHigh() - m->getLevelLow() <= power)
			m->on();
}

unsigned int Platform::totalPower()							//��������� ��������
{
	unsigned int summ = 0;
	for each(Module* m in modules)
		summ += m->tempLevel();
	return summ;
}

list <Cell*> Platform::getInfo(const Environment& E)		//��������� ����������
{
	list <Cell*> totalList = list <Cell*>();
	for each (Module* m in modules)
	{
		if (typeid(*m) == typeid(Sensor) || typeid(*m) == typeid(Ray) || typeid(*m) == typeid(Optic))
		{
			list <Cell*> ls = ((Sensor*)m)->getInfo(x, y, E);
			for each (Cell* c in ls)
				totalList.push_back(c);
		}
		if (typeid(*m) == typeid(Net))
		{
			list <Cell*> ls = ((Net*)m)->getInfo(E);
			for each (Cell* c in ls)
				totalList.push_back(c);
		}
	}
	totalList.sort();
	totalList.unique();
	return totalList;
}

bool Platform::addSlave(Cell* C)							//�������� ������������
{
	for each (Module* m in modules)
		if (typeid(*m) == typeid(Net))
			if (((Net*)m)->add(x, y, C,this))
				return true;
	return false;
}

void Platform::destroy(Cell* C)								//������ ������ �� ����������� ��������� � ��� ������� - �����������
{
	for each (Module* m in modules)
	{
		if(typeid(*m) == typeid(Arms))
			if (((Arms*)m)->canDestroy(x, y, C))
			{
				((Arms*)m)->destroy(x, y, C);
				return;
			}
	}
	for each (Module* m in modules)
	{
		if (typeid(*m) == typeid(Net))
			((Net*)m)->destroy(x, y, C);
	}
}

void Platform::correctTime()
{
	for each (Module* m in modules)
		if (typeid(*m) == typeid(Arms))
			((Arms*)m)->correctTime();
}

void Platform::addSession(Cell* c)							//�������� ������������ ������
{
	for each (Module* m in modules)
		if (typeid(*m) == typeid(Net))
			((Net*)m)->addSession(x, y, c);
}

list <Cell*> Platform::getRobots()									//��������� ������ �������
{
	list <Cell*> allRobots = list<Cell*>();
	for each (Module* m in modules)
		if (typeid(*m) == typeid(Net))
		{
			list <Robot*> robots = ((Net*)m)->getRobots();
			for each (Robot* robot in robots)
				allRobots.push_back(robot);
		}
	allRobots.sort();
	allRobots.unique();
	return allRobots;
}

bool Platform::willWorked(unsigned int x, unsigned int y, Platform* p)		//����� �� ����������� ������������ ������
{
	for each(Module* m in modules)
		if (typeid(*m) == typeid(Net) && ((Net*)m)->consist((Robot*)p))
			return dist(x,y) <= m->getRadius();
	return true;
}

bool Platform::safe(unsigned int x, unsigned int y)					//���������� �� ��������� ������������ ������ ���������
{
	for each (Module* m in modules)
		if (typeid(*m) == typeid(Arms) && dist(x, y) <= m->getRadius())
			return false;
	return true;
}

void Platform::correctNet(Cell* c)
{
	Robot* r = (Robot*)c;
	for each (Module* m in modules)
	{
		if (typeid(*m) == typeid(Net))
		{
			((Net*)m)->correctNet((Robot*)c, x, y);
		}
	}
}

void Platform::remove(Cell* c)
{
	for each (Module* m in modules)
		if (typeid(*m) == typeid(Net))
			((Net*)m)->remove(c);
}

bool Platform::netExist(unsigned int x, unsigned int y)
{
	for each (Module* m in modules)
		if (typeid(*m) == typeid(Net))
			if (((Net*)m)->getRadius() >= distance(x, y, getX(), getY()))
				return true;
	return false;
}

unsigned int Platform::distance(unsigned int x, unsigned int y, unsigned int X, unsigned int Y)
{
	if (abs((int)x - (int)X) > abs((int)y - (int)Y))
		return abs((int)x - (int)X);
	else
		return abs((int)y - (int)Y);
}