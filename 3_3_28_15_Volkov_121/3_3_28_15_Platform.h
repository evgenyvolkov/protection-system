#pragma once

#define vector Vector
#include <sstream>
#include <list>
#include "3_3_28_15_Environment.h"
#include "3_3_28_15_Module.h"
#include "Vector.h"

using namespace std;

const unsigned int SIZE = 5;
//!����� - ���������, ����������� �� ������(Cell)
class Platform :
	public Cell
{

protected:
	vector<Module*> modules;									///<������ ������� (����� � ������, �� ������ ����� �.�. ������ ����������� � ��������� �����)
	unsigned int power;											///<�������������� ��������
public:

	Platform(); ///<������ �����������
	Platform(unsigned int x, unsigned int y, unsigned int p) : Cell(x, y) { power = p;} ///<����������� \param x ���������� ������� ������ \param y ���������� ������� ������ \param p ��������
	~Platform();     ///<����������
	Platform(const Platform& P);								///<���������� ����������� \param P ������ �� ���������
	Platform(Platform&& P);										///<������������ ����������� \param P ������� ������ �� ���������
	Platform& operator= (const Platform& P);						///<���������� �������� ������������ \param P ������ �� ��������� 
	Platform& operator= (Platform&& P);							///<������������ �������� ������������ \param P ������� ������ �� ��������� 

	Cell* clone() const { return new Platform(*this); }			///<�������� ����� ������� \return ��������� �� ��������� 
	void add(Module* M) { modules.push_back(M); }				///<���������� ������ \param M ��������� �� ������
	unsigned int getFreePlace() const;							///<��������� ���������� ���������� ����� \return ��������
	unsigned int getCount() const {	return modules.size(); }	///<��������� ����� ������� \return ��������
	void remove (unsigned int i);								///<�������� ������ �� ������� \param i ������
	void correctPower();										///<������������� ������������ ��������
	unsigned int totalPower();									///<�������� ������������ �������� \return ��������
	virtual list <Cell*> getInfo(const Environment& E);			///<��������� ���������� �� ��������� �� ���� � �������� \param E ������ �� ����� \return ������ ��������
	bool addSlave(Cell* cell);										///<�������� ������������ - ������ \param cell ��������� �� ������ \return ������ ��������
	virtual char print() { return 'P'; }						///<����� ������� �������(��� �������) \return ������(char)
	virtual string toStr() const;							///<��������� ������ ���������� \return ������
	virtual void destroy(Cell* cell);								///<������ �� ����������� \param cell ��������� �� ������
	void correctTime();											///<������������� �������
	void addSession(Cell* cell);										///<���������� ������ \param cell ��������� �� ������
	list <Cell*> getRobots();									///<��������� ������ ������� \return ������ ��������
	bool willWorked(unsigned int x, unsigned int y, Platform* p);	///<��������, �������� �� ���������� ������� �������� ����� ����������� � �������� ����� \param x ���������� ������� ������ \param y ���������� ������� ������ \param p ��������� �� ��������� \return ������ ��������
	virtual bool safe(unsigned int x, unsigned int y);					///<������� ������������ ������ ����� ������������ ������ ��������� \param x ���������� ������� ������ \param y ���������� ������� ������ \return ������ ��������
	vector<Module*> getModules() { return modules; }			///<��������� ������� ������������� ������� \return ������ �������
	unsigned int getPower() const { return power; }				///<��������� �������� �������� \return ��������
	void correctNet(Cell*);		///<������������� ������ \param c ��������� �� ������
	void remove(Cell*);			///<�������� ������� ��� ������ ����� \param c ��������� �� ������
	bool netExist(unsigned int x, unsigned int y); ///<�������� �������� ������ \param x ���������� ������� ������ \param y ���������� ������� ������ \return ������ ��������
	unsigned int distance(unsigned int x, unsigned int y, unsigned int X, unsigned int Y); ///<���������� ����� ����� ������� \param x ���������� ������� 1 ������ \param y ���������� ������� 1 ������ \param X ���������� ������� 2 ������ \param Y ���������� ������� 2 ������ \return ��������

};

