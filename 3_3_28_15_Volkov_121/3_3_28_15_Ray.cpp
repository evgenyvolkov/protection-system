#include "stdafx.h"
#include "3_3_28_15_Ray.h"

string Ray::toString() const
{
	ostringstream ost;
	ost << "Sensor:" << " levelHigh = " << levelHigh << " levelLow = " << levelLow << " radius = " << radius << " size = " << size << " type = ray";
	return ost.str();
}

list <Cell*> Ray::getInfo(unsigned int x, unsigned int y, const Environment& E)
{
	//��������� ���������
	return E.getInfo(x, y, radius);
}