#pragma once
#include "3_3_28_15_Sensor.h"
//!����� - ������������� ������, ����������� �� �������(Sensor)
class Ray :
	public Sensor
{
public:
	Ray() : Sensor() { } ///<������ �����������
	Ray(unsigned int rad, unsigned int energyH, unsigned int energyL, unsigned int size) : Sensor(rad, energyH, energyL, size) { }  ///<����������� \param rad ������ �������� \param energyH ������� ����������� ������� � �������� ������ \param energyL ������� ����������� ������� � ��������� ������ \param size ��������� ���������� ������ 
	~Ray() {} ///<����������

	virtual Module* clone() { return new Ray(*this); } ///<�������� ����� ������� \return ��������� �� ������
	virtual string toString() const; ///<��������� ������ ���������� \return ������
	virtual list <Cell*> getInfo(unsigned int x, unsigned int y, const Environment& E);	///<������������� ������� ��������� ������ ���������� \param x ���������� ������� ������ \param y ���������� ������� ������ \param E ������ �� ����� \return ������ ����������
};

