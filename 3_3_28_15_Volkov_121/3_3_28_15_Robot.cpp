#include "stdafx.h"
#include "3_3_28_15_Robot.h"
#include "3_3_28_15_Optic.h"
#include "3_3_28_15_Ray.h"
#include "3_3_28_15_Cell.h"
#include "3_3_28_15_Net.h"
#include "3_3_28_15_Arms.h"

Robot::Robot()
{
	power = 1;
	speed = 1;
	aimX = 0;
	aimY = 0;
	x = 0;
	y = 0;
	modules = vector<Module*>();
}


Robot::~Robot()
{
	for (unsigned int i = 0; i < modules.size(); i++)
		delete modules[i];
}

Robot::Robot(const Robot& R)									//���������� �����������
{
	power = R.power;
	speed = R.speed;
	aimX = R.aimX;
	aimY = R.aimY;
	x = R.x;
	y = R.y;
	modules = vector<Module*>();
	for (unsigned int i = 0; i < R.modules.size(); i++)
		modules.push_back(R.modules[i]->clone());
}
Robot::Robot(Robot&& R)									//������������ �����������
{
	power = R.power;
	speed = R.speed;
	aimX = R.aimX;
	aimY = R.aimY;
	x = R.x;
	y = R.y;
	modules = vector<Module*>();
	for (unsigned int i = 0; i < R.modules.size(); i++)
	{
		modules.push_back(R.modules[i]);
		R.modules[i] = NULL;
	}
}

Robot& Robot::operator= (const Robot& R)						//���������� �������� ������������
{
	if (this != &R)
	{
		power = R.power;
		speed = R.speed;
		aimX = R.aimX;
		aimY = R.aimY;
		x = R.x;
		y = R.y;
		modules = vector<Module*>();
		for (unsigned int i = 0; i < R.modules.size(); i++)
			modules.push_back(R.modules[i]->clone());
	}
	return *this;
}
Robot& Robot::operator= (Robot&& R)							//������������ �������� ������������
{
	if (this != &R)
	{
		power = R.power;
		speed = R.speed;
		aimX = R.aimX;
		aimY = R.aimY;
		x = R.x;
		y = R.y;
		modules = vector<Module*>();
		for (unsigned int i = 0; i < R.modules.size(); i++)
		{
			modules.push_back(R.modules[i]);
			R.modules[i] = NULL;
		}
	}
	return *this;
}

string Robot::toStr() const
{
	ostringstream ost;
	ost << "Robot: " << (enabled?'Y':'N') << "  (" << x << ";" << y << ") Free place = " << getFreePlace() << ", power = " << power << ", speed = " << speed << endl;
	ost << "  Aim  = (" << aimX << ";" << aimY << ")" << endl;
	if (parent)
		ost << "  Parent = (" << parent->getX() << ";" << parent->getY() << ")" << endl;
	for (unsigned int i = 0; i < modules.size(); i++)
		ost << "M " << i << " : " << (modules[i]->getEnabled() ? " on " : "off ") << *modules[i] << endl;
	return ost.str();
}

list <Cell*> Robot::getInfo(const Environment& E)		//�������� ���������� � ������ � ��� �����������
{
	list <Cell*> totalList = list <Cell*>();
	if(enabled)
		for each (Module* m in modules)
		{
			if (typeid(*m) == typeid(Sensor) || typeid(*m) == typeid(Ray) || typeid(*m) == typeid(Optic))
			{
				list <Cell*> ls = ((Sensor*)m)->getInfo(x, y, E);
				for each (Cell* c in ls)
					totalList.push_back(c);
			}
			if (typeid(*m) == typeid(Net))
			{
				list <Cell*> ls = ((Net*)m)->getInfo(E);
				for each (Cell* c in ls)
					totalList.push_back(c);
			}
		}
	totalList.sort();
	totalList.unique();
	return totalList;
}

void Robot::destroy(Cell* C)		//������ �� �����������
{
	if (enabled)
	{
		for each (Module* m in modules)
		{
			if (typeid(*m) == typeid(Arms))
				if (((Arms*)m)->canDestroy(x, y, C))
				{
					((Arms*)m)->destroy(x, y, C);
					return;
				}
		}
	}
}

void Robot::move(Environment& E)
{
	int dx = min((int) speed, abs((int)aimX - (int)x));
	int dy = min((int)speed, abs((int)aimY - (int)y));
	while (dx != 0 || dy != 0)
	{								//�������� ������������� � ����� �� �����
		if (!E.exist(x + sign((int)aimX - (int)x), y) && dx != 0 && canMove(x + sign((int)aimX - (int)x), y) && E.netExist(x + sign((int)aimX - (int)x), y))
		{
			dx--;
			if (E.correct(x + sign((int)aimX - (int)x), y))
				x = x + sign((int)aimX - (int)x);
		}
		else if (!E.exist(x, y + sign((int)aimY - (int)y)) && dy != 0 && canMove(x, y + sign((int)aimY - (int)y)) && E.netExist(x, y + sign((int)aimY - (int)y)))
		{
			dy--;
			if (E.correct(x, y + sign((int)aimY - (int)y)))
				y = y + sign((int)aimY - (int)y);
		}
			return;
	}
}

int Robot::sign(int i)
{
	if (i < 0)
		return -1;
	if (i > 0)
		return 1;
	return 0;
}

int Robot::min(int n1, int n2)
{
	if (n1 < n2)
		return n1;
	return n2;
}

bool Robot::canMove(unsigned int x, unsigned int y)		//���������, ����� �� ������������� �� ������� �����
{
	//if (parent && !parent->willWorked(x, y, this))
		//return false;
	for each(Module* m in modules)
		if (typeid(*m) == typeid(Net))
			if (!((Net*)m)->willWorked(x, y))
				return false;
	return true;
}

void Robot::correctNet()
{
	if (parent)
	{
		parent->correctNet(this);
	}
	else
	{
		enabled = false;
	}
}

bool Robot::safe(unsigned int x, unsigned int y)					//���������� �� ��������� ������������ ������ ���������
{
	if (!enabled)
		return true;
	for each (Module* m in modules)
		if (typeid(*m) == typeid(Arms) && dist(x, y) <= m->getRadius())
			return false;
	return true;
}