#pragma once
#include "3_3_28_15_Platform.h"
#include "3_3_28_15_Environment.h"

//!����� - �����(��������� ���������), ����������� �� ���������(Platform)
class Robot :											
	public Platform
{
private:

	unsigned int speed;									///<��������
	bool enabled;										///<����� �������� ��� ���
	unsigned int aimX;									///<���������� �������� ���� 
	unsigned int aimY;									///<���������� �������� ���� 
	Platform* parent;									///<����������� ����� ��� ���������
public:
	Robot(); ///<������ �����������
	~Robot();  ///<����������
	Robot(const Robot& R);								//���������� ����������� \param R ������ �� ������
	Robot(Robot&& R);										//������������ ����������� \param R ������� ������ �� ������
	Robot(unsigned int x, unsigned int y, unsigned int p, unsigned int s) : Platform(x, y, p), speed(s), aimX(x), aimY(y) { enabled = false; parent = NULL; }
	
	Robot& operator= (const Robot& R);					///<���������� �������� ������������ \param R ������ �� ������
	Robot& operator= (Robot&& R);						///<������������ �������� ������������ \param R ������� ������ �� ������
	void move(Environment& E);							///<������� �������� ������� \param E ������ �� �����

	Cell* clone() const { return new Robot(*this); }	///<�������� ����� ������� \return ��������� �� ������
	virtual char print() { return 'R'; }				///<����� ������� �������(��� �������) \return ������(char)
	bool enable() { return enabled; }					///<�������� ��������� ������ \return ������ ��������
	void connect(Platform* p) { enabled = true; parent = p; }					///<����������� � ��������� - ����� �������� \param p ��������� �� ���������
	void disconnect() { enabled = false; parent = NULL; }				///<���������� �� ���������
	virtual string toStr() const;												///<��������� ������ ���������� \return ������
	virtual list <Cell*> getInfo(const Environment& E);	///<��������� ���������� �� ��������� �� ���� � �������� \param E ������ �� ����� \return ������ ��������
	virtual void destroy(Cell* cell);						///<����������� ������ \param cell ��������� �� ������
	void setAim(unsigned int x, unsigned int y) { aimX = x; aimY = y; }	///<���������� ������ ����� ����, ���� �� ������ ���������, ���� ����� \param x ���������� ������� ������ \param y ���������� ������� ������
	static int sign(int n);								///<���� ����� \param n ����� \return ����
	static int min(int n1, int n2);						///<����������� ����� \param n1 1 ����� \param n2 2 ����� \return ����������� �����
	bool canMove(unsigned int x, unsigned int y);		///<��������, ����� �� ����� ��������� � ������ ����� � �� �������� ���������� � ����� � ���������� \param x ���������� ������� ������ \param y ���������� ������� ������ \return ������ ��������
	unsigned int getSpeed() const { return speed; }		///<��������� �������� �������� ������ \return ��������
	void correctNet();									///<������������� ������
	virtual bool safe(unsigned int x, unsigned int y);  ///<�������� ������������ ��������� ������������ ������� ������ \param x ���������� ������� ������ \param y ���������� ������� ������ \return ������ ��������
};

