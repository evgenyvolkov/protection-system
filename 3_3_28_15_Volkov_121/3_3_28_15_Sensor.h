#pragma once

#include "3_3_28_15_Module.h"
#include "3_3_28_15_Environment.h"
#include <string>
#include <sstream>
using namespace std;
//!����� - ������(������������), ����������� �� ������
	
class Sensor :
	public Module
{
private:

public:
	Sensor() : Module() { }		///<������ �����������
	Sensor(unsigned int rad, unsigned int energyH, unsigned int energyL, unsigned int size) : Module(rad, energyH, energyL, size) { } ///<����������� \param rad ������ �������� \param energyH ������� ����������� ������� � �������� ������ \param energyL ������� ����������� ������� � ��������� ������ \param size ��������� ���������� ������ 
	virtual list <Cell*> getInfo(unsigned int x, unsigned int y, const Environment& E) = 0; ///<������� ��������� ������ ���������� \param x ���������� ������� ������ \param y ���������� ������� ������ \param E ������ �� ����� \return ������ ����������
	~Sensor() {} ///<����������
	virtual Module* clone() = 0; ///<�������� ����� ������� \return ��������� �� ������
	virtual string toString() const = 0;  ///<��������� ������ ���������� \return ������
};

