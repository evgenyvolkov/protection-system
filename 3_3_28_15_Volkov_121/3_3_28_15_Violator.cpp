#include "stdafx.h"
#include "3_3_28_15_Violator.h"


std::string Violator::toStr() const
{
	ostringstream ost;
	ost << "Violator: (" << x << ";" << y << "), speed = " << speed << ", death = " << death << endl;
	return ost.str();
}


int Violator::sign(int i)
{
	if (i < 0)
		return -1;
	if (i > 0)
		return 1;
	return 0;
}

int min(int n1, int n2)
{
	if (n1 < n2)
		return n1;
	return n2;
}

void Violator::move(Environment& E)
{
	int dx = min((int)speed, abs((int)aimX - (int)x));						//�������� ��������� ���������� ������
	int dy = min((int)speed, abs((int)aimY - (int)y));
	while (dx != 0 || dy != 0)
	{
		if (!E.exist(x + sign((int)aimX - (int)x), y) && dx != 0 && E.correct(x + sign((int)aimX - (int)x),y))
		{
			dx--;
			if (E.correct(x + sign((int)aimX - (int)x), y))
				x = x + sign((int)aimX - (int)x);
		}
		else if (!E.exist(x, y + sign((int)aimY - (int)y)) && dy != 0 && E.correct(x, y + sign((int)aimY - (int)y)))
		{
			dy--;
			if (E.correct(x, y + sign((int)aimY - (int)y)))
				y = y + sign((int)aimY - (int)y);
		}
		else
			return;
	}
}
