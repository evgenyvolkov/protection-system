#pragma once
#include "3_3_28_15_Cell.h"
#include "3_3_28_15_Environment.h"
#include <sstream>

//!����� - ����������, ����������� �� ������(Cell)
class Violator :
	public Cell
{
private:
	unsigned int speed; ///<��������
	unsigned aimX;   ///<�������� ����
	unsigned aimY;   ///<�������� ����
public:
	Violator() {} ///<������ �����������
	Violator(unsigned int x, unsigned int y, unsigned int s) : Cell(x, y), speed(s) { aimX = x; aimY = y; } ///<������� ��������� ������ ���������� \param x ���������� ������� ������ \param y ���������� ������� ������ \param s ��������
	~Violator() {} ///<����������

	Cell* clone() const { return new Violator(*this); }	///<�������� ����� ������� \return ��������� �� ����������
	virtual char print() { return 'V'; }				///<����� ������� �������(��� �������) \return ������(char)
	virtual std::string toStr() const;					///<��������� ������ ���������� \return ������

	void move(Environment& E);	///<������� ����������� ������� � �������� ����� \param E ������ �� �����
	unsigned int getSpeed() { return speed; }			///<��������� �������� ���������� \return �������� 
	void setAimX(unsigned x) { aimX = x; }				///<���������� �������� ����� ���� \param x �������� ����
	void setAimY(unsigned y) { aimY = y; }				///<���������� �������� ����� ���� \param x �������� ����
	static int sign(int i);								///<���� ����� \param i ����� \return ����
};

