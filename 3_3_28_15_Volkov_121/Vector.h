#pragma once

#include <exception>

//!����������� ������ - ������
template <class T>
class Vector
{

private:
	T* items;											///<��������
	unsigned int cp;									///<������������ ������� �����
	unsigned int sz;									///<������� ������
public:

	class iterator;										///<��������� ����� ���������
	class const_iterator;								///<���������� ����� ������������ ���������

	Vector();											///<������ ������������
	~Vector();											///<����������
	Vector(const Vector<T>& V);					 ///<���������� ����������� \param V ������ �� ������
	Vector(Vector<T>&& V);						///<������������ ����������� \param V ������� ������ �� ������

	Vector<T>& operator= (const Vector<T>& V);			///<���������� �������� ������������ \param V ������ �� ������
	Vector<T>& operator= (Vector<T>&& V);				///<������������ �������� ������������ \param V ������� ������ �� ������
	T& operator[] (unsigned int i);						///<������������� �������� �������������� \param i ������ \return ������ �� ������
	const T& operator[] (unsigned int i) const;        ///<������������� �������� �������������� \param i ������ \return ������ �� ������
	bool operator != (const Vector<T>& V) const;		///<������������� �������� ����������� \param V ������ �� ����� \return ������ ��������
	bool operator == (const Vector<T>& V) const;		///<������������� �������� ��������� \param V ������ �� ����� \return ������ ��������

	unsigned int capacity() const;						///<��������� ������� ������������ ����������� \return ��������
	unsigned int size() const;							///<��������� �������� ������� \return ��������
	void push_back(const T& V);							///������� �������� � ����� ������� \param V ������ �� ������

	void erase(iterator it)								///<���������� �� ��������� \param it ��������
	{
		if (it.V == *this)
			for (unsigned int i = it.cur; i < sz - 1; i++)
				items[i] = items[i + 1];
		sz--;
	}

	const_iterator begin() const { return const_iterator(*this, 0); }		///<��������� ������������ ��������� �� ������ �������
	const_iterator end() const { return const_iterator(*this, size()); }    ///<���������  ������������ ��������� �� ����� �������
	 
	//!�����-����������� ��������
	class const_iterator
	{
	private:
		
		const Vector<T>& V; ///!��������������� ������
		
		unsigned int cur; ///<��������� ���������
	public:
		
		const_iterator(const Vector<T>& V, unsigned int cur) :V(V), cur(cur) {} ///<����������� \param V ������ �� ������ \param cur ������� ��������� ���������
		
		const_iterator& operator++()///<������������� �������� ���������� \return ������ �� �����. ��������
		{
			++cur;
			return *this;
		}

		const_iterator operator++(int) ///<������������� �������� ���������� \return �����. ��������
		{
			const_iterator it(*this);
			++cur;
			return it;
		}

		const_iterator operator+(int i) { ///<������������� �������� + \param i �������� \return �����. ��������
			iterator it(*this);
			it.cur += i;
			return it;
		}
		
		bool operator!= (const const_iterator& I) const ///<�������� �� ����������� \param I ����������� �������� \return ������ ��������
		{
			return V != I.V || cur != I.cur;
		}
	
		const T& operator*() ///<������������� �������� ��������� �������� �� ��������� \return ������ �� ����������� �������� 
		{
			return const V[cur];
		}
	};		

	iterator begin() { return iterator(*this, 0); } ///<��������� ��������� �� ������ �������
	iterator end() { return iterator(*this, size()); } ///<��������� ��������� �� ����� �������

	//!����� - ��������
	class iterator
	{
		friend Vector;
	private:
		Vector<T>& V; ///!��������������� ������
		unsigned int cur; ///<��������� ���������
	public:
		iterator(Vector<T>& V, unsigned int cur) :V(V), cur(cur) {}  ///<����������� \param V ������ �� ������ \param cur ������� ��������� ���������

		iterator& operator++() ///<������������� �������� ���������� \return ������ �� ��������
		{
			++cur;
			return *this;
		}

		iterator operator++(int) ///<������������� �������� ���������� \return ��������
		{
			iterator it(*this);
			++cur;
			return it;
		}

		iterator operator+(int i) { ///<������������� �������� + \param i �������� \return ��������
			iterator it(*this);
			it.cur += i;
			return it;
		}

		bool operator!= (const iterator& I) const ///<�������� �� ����������� \param I �������� \return ������ ��������
		{
			return V != I.V || cur != I.cur;
		}

		T& operator*() ///<������������� �������� ��������� �������� �� ��������� \return ������ �� �������� 
		{
			return V[cur];
		}
	};

};

//����� ������ �������
//������������ � �����������
template <class T>
Vector<T>::Vector()
{
	items = new T[0];
	cp = 0;
	sz = 0;
}

template <class T>
Vector<T>::~Vector()
{
	delete[] items;
}

template <class T>
Vector<T>::Vector(const Vector<T>& V)
{
	items = new T[V.cp];
	for (unsigned int i = 0; i < V.sz; i++)
		items[i] = V.items[i];
	sz = V.sz;
	cp = V.cp;
}

template <class T>
Vector<T>::Vector(Vector<T>&& V)
{
	items = V.items;
	sz = V.sz;
	cp = V.cp;
	V.items = NULL;
}
//��������� ������������
template <class T>
Vector<T>& Vector<T>::operator= (const Vector<T>& V)
{
	if (this != &V)
	{
		items = new T[V.cp];
		for (unsigned int i = 0; i < V.sz; i++)
			items[i] = V.items[i];
		sz = V.sz;
		cp = V.cp;
	}
	return *this;
}

template <class T>
Vector<T>& Vector<T>::operator= (Vector<T>&& V)
{
	if (this != &V)
	{
		items = V.items;
		sz = V.sz;
		cp = V.cp;
		V.items = NULL;
	}
	return *this;
}
//�������������� � ����������� �������� ���������� ��������
template <class T>
T& Vector<T>::operator[] (unsigned int i)
{
	if (i >= sz)
		throw out_of_range("");
	else
		return items[i];
}

//�������������� ��� ���������� ������ ��������
template <class T>
const T& Vector<T>::operator[] (unsigned int i) const
{
	if (i >= sz)
		throw out_of_range("");
	else
		return items[i];
}
//�������� !=
template <class T>
bool Vector<T>::operator!= (const Vector<T>& V) const
{ 
	return items != V.items; 
}
//�������� ==
template <class T>
bool Vector<T>::operator== (const Vector<T>& V) const
{
	return items == V.items;
}
//��������� �����������
template <class T>
unsigned int Vector<T>::capacity() const
{
	return cp;
}
//� �������
template <class T>
unsigned int Vector<T>::size() const
{
	return sz;
}
//������ � �����
template <class T>
void Vector<T>::push_back(const T& item)
{
	if (cp == sz)
	{
		int cpNew = cp;
		if (cp/2 == 0)
			cpNew++;
		else
			cpNew += cp / 2;
		T* itemsNew = new T[cpNew]();
		for (unsigned int i = 0; i < cp; i++)
			itemsNew[i] = items[i];
		itemsNew[cp] = item;
		cp = cpNew;
		sz++;
		delete[] items;
		items = itemsNew;
	}
	else
		items[sz++] = item;
}
