// 3_3_28_15_Volkov_121_app.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "..\3_3_28_15_Volkov_121\3_3_28_15_Game.h"

#include <iostream>
#include <string>
#include <exception>

using namespace std;

string msgs[] = { "Exit", "Print menu", "Print field", "Get field size", "Set field size", "Add bar", "Add violator", "Add platform", "Add robot", 
				"Delete cell", "Add net module", "Add sensor module", "Add arms", "Delete module","GO!!!", "Get information from AI", "Get information from XY"  };
const int amount = 17;

Game* CreateNew();
void GetCoor(unsigned int& x, unsigned int& y);


int Exit(Game*);							//������� ������
int Menu(Game*);							//����
int Print(Game*);
int GetSize(Game*);
int SetSize(Game*);
int AddBar(Game*);
int AddViolator(Game*);
int AddPlatform(Game*);
int AddRobot(Game*);
int DeleteCell(Game*);
int AddNet(Game*);
int AddSensor(Game*);
int AddArms(Game*);
int DeleteModule(Game*);
int Play(Game*);
int GetInfoAI(Game*);
int GetInfoXY(Game*);

unsigned int getUInt();								//������� ���������� ������ �����

int main()
{

	srand(time_t());
	Game* G;								//������ �� 3 �������������������
	try
	{
		G = CreateNew();
	}
	catch (bad_alloc)												//�������� ����������, ���������� � ��������� ������
	{
		cout << "Out of memory. :-(" << endl;
		return 1;
	}
	int ex = 0;														//��������� ��� �������� ������ ����
	char choice = 0;												//����� �� ����� - ����� ����
	for (int i = 0; i<amount; ++i)
	{
		cout << (char)(i + 'a') << " " << msgs[i] << ", ";
	}
	int(*func[amount]) (Game*) = { Exit, Menu, Print, GetSize, SetSize, AddBar, AddViolator, AddPlatform, AddRobot, 
		DeleteCell, AddNet, AddSensor, AddArms, DeleteModule, Play, GetInfoAI, GetInfoXY};					//������ �������

	while (!ex)
	{																//���� ������ ������
		cout << "\nMake your choice: ";
		cin >> choice;
		while (!((choice>('a' - 1)) && ((choice<amount + 'a'))))	//���� �� ����� ���������� ����� ������
			cin >> choice;
		if ((choice>('a' - 1)) && ((choice<amount + 'a')))
			try
		{
			ex = (*func[choice - 'a'])(G);							//������� ��������, ������� ������� ��������
		}
		catch (bad_alloc)
		{
			ex = 1;
			cout << "Out of memory!" << endl;
		}
	}

	return 0;
}

//������� �������� ����� ������. ������ 'a' ����� ������� � ������� ������� ������������, 'b' - ������� � ������� ������������ - � ����� ������, 'c' - ������� ��������� ������
Game* CreateNew()
{
	unsigned int width = 0;
	unsigned int height = 0;
	while (width <= 0)
	{
		cout << "Enter width of field." << endl;
		width = getUInt();
	}
	while (height <= 0)
	{
		cout << "Enter height of field." << endl;
		height = getUInt();
	}
	return new Game(width, height);
}

int Exit(Game* G) 													//���������� ���������
{
	delete G;														//������� ������, ��������� ��� ���������� �� ������������������
	return 1;
}

int Menu(Game*)														//����� ����
{
	int i;
	printf("\n");
	for (i = 0; i < amount; ++i)
		cout << (char)(i + 'a') << " " << msgs[i] << ", ";
	return 0;
}

int Print(Game* G)
{
	cout << *G;
	return 0;
}

int GetSize(Game* G)
{
	cout << "Width = " << G->getFieldWidth() << "; Height = " << G->getFieldHeight() << endl;
	return 0;
}



int SetSize(Game* G)
{
	GetSize(G);
	unsigned int width = 0;
	unsigned int height = 0;
	while (width <= 0)
	{
		cout << "Enter width of field." << endl;
		width = getUInt();
	}
	while (height <= 0)
	{
		cout << "Enter height of field." << endl;
		height = getUInt();
	}
	G->setFieldWidth(width);
	G->getFieldHeight(height);
	cout << "Size changed" << endl;
	GetSize(G);
	return 0;
}

int AddBar(Game* G)
{
	unsigned int x = 0;
	unsigned int y = 0;
	GetCoor(x, y);
	try
	{
		cout << "new bar is: " << *(G->addBar(x, y)) << endl;
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int AddViolator(Game* G)
{
	unsigned int x = 0;
	unsigned int y = 0;
	unsigned int speed = 0;
	GetCoor(x, y);
	cout << "Enter speed of violator" << endl;
	speed = getUInt();
	try
	{
		cout << "new violator is: " << *(G->addViolator(x, y, speed)) << endl;
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int AddPlatform(Game* G)
{
	unsigned int x = 0;
	unsigned int y = 0;
	GetCoor(x, y);
	cout << "Enter power" << endl;
	unsigned int power = getUInt();
	try
	{
		cout << "new platform is: " << *(G->addPlatform(x, y, power)) << endl;
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int AddRobot(Game* G)
{
	unsigned int x = 0;
	unsigned int y = 0;
	unsigned int speed = 0;
	GetCoor(x, y);
	cout << "Enter power" << endl;
	unsigned int power = getUInt();
	cout << "Enter speed of robot" << endl;
	speed = getUInt();

	try
	{
		cout << "new robot is: " << *(G->addRobot(x, y, power, speed)) << endl;
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int DeleteCell(Game* G)
{
	unsigned int x = 0;
	unsigned int y = 0;
	GetCoor(x, y);
	try
	{
		G->deleteCell(x, y);
		cout << "(" << x << ";" << y << ") deleted" << endl;
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int AddNet(Game* G)
{
	unsigned int x = 0;
	unsigned int y = 0;
	GetCoor(x, y);
	cout << "Enter radius" << endl;
	unsigned int rad = getUInt();
	cout << "Enter high energy consumption" << endl;
	unsigned int energyH = getUInt();
	cout << "Enter low energy consumption" << endl;
	unsigned int energyL = getUInt();
	cout << "Enter size" << endl;
	unsigned int size = getUInt();
	cout << "Enter max count of sessions" << endl;
	unsigned int count = getUInt();
	try
	{
		G->addNet(x, y, rad, energyH, energyL, size, count);
		cout << "Module added" << endl;
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int AddSensor(Game* G)
{
	unsigned int x = 0;
	unsigned int y = 0;
	GetCoor(x, y);
	cout << "Enter radius" << endl;
	unsigned int rad = getUInt();
	cout << "Enter high energy consumption" << endl;
	unsigned int energyH = getUInt();
	cout << "Enter low energy consumption" << endl;
	unsigned int energyL = getUInt();
	cout << "Enter size" << endl;
	unsigned int size = getUInt();
	cout << "Enter '0' - to optical, '1' - to x-ray" << endl;
	char c = '0';
	cin >> c;
	try
	{
		G->addSensor(x, y, rad, energyH, energyL, size, c!='0');
		cout << "Module added" << endl;
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int AddArms(Game* G)
{
	unsigned int x = 0;
	unsigned int y = 0;
	GetCoor(x, y);
	cout << "Enter radius" << endl;
	unsigned int rad = getUInt();
	cout << "Enter high energy consumption" << endl;
	unsigned int energyH = getUInt();
	cout << "Enter low energy consumption" << endl;
	unsigned int energyL = getUInt();
	cout << "Enter size" << endl;
	unsigned int size = getUInt();
	cout << "Enter time" << endl;
	unsigned int time = getUInt();
	try
	{
		G->addArms(x, y, rad, energyH, energyL, size, time);
		cout << "Module added" << endl;
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int DeleteModule(Game* G)
{
	unsigned int x = 0;
	unsigned int y = 0;
	GetCoor(x, y);
	cout << "Enter nu,ber of module" << endl;
	unsigned int  i= getUInt();
	try
	{
		G->deleteModule(x, y, i);
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int Play(Game* G)
{
	cout << *G;
	G->play();
	cout << *G;
	return 0;
}

int GetInfoAI(Game* G)
{
	list <Cell*> ls = G->getInfoAI();
	for each (Cell* c in ls)
		cout << *c;
	return 0;
}

int GetInfoXY(Game* G)
{
	unsigned int x = 0;
	unsigned int y = 0;
	GetCoor(x, y);
	list <Cell*> ls = G->getInfoXY(x,y);
	if (ls.size() == 0)
		cout << "No information";
	for each (Cell* c in ls)
		cout << *c;
	return 0;
}

unsigned int getUInt()														//��������� ������ �����, 
{
	int n = -1;
	do																//���� �� ����� ������ ����������
	{
		if (!cin.good())
		{
			cin.clear();												//������� ������ 
			cin.ignore();												//������������� ������
		}
		cin >> n;
	} while (!cin.good() || n < 0);											//���� ����� �� good(), �� ���������� ��������, ������� ����� �����, ��������������� �����
	return (unsigned int)n;
}

void GetCoor(unsigned int& x, unsigned int& y)
{
	cout << "Enter x." << endl;
	x = getUInt();
	cout << "Enter y." << endl;
	y = getUInt();
}


