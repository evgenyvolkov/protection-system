#pragma once
#include "..\3_3_28_15_Volkov_121\3_3_28_15_Game.h"
#include "..\3_3_28_15_Volkov_121\3_3_28_15_Arms.h"
#include "..\3_3_28_15_Volkov_121\3_3_28_15_Sensor.h"
#include "..\3_3_28_15_Volkov_121\3_3_28_15_Optic.h"
#include "..\3_3_28_15_Volkov_121\3_3_28_15_Ray.h"

namespace GUI 
{

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;

	/// <summary>
	/// ������ ��� MainForm
	/// </summary>
	public ref class MainForm : public System::Windows::Forms::Form
	{

	private: 
		Game* G;
	public:
		MainForm(void)
		{
			InitializeComponent();
			srand(time_t());
			G = new Game(1,1);
			//NewGame();
			
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MainForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  buttonStart;
	protected:


	private: System::Windows::Forms::Panel^  panel1;
	private: System::Windows::Forms::Button^  buttonLoad;
	private: System::Windows::Forms::TextBox^  textBoxPath;
	private: System::Windows::Forms::Button^  buttonTest;
	private: System::Windows::Forms::Button^  button1;


	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->buttonStart = (gcnew System::Windows::Forms::Button());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->buttonLoad = (gcnew System::Windows::Forms::Button());
			this->textBoxPath = (gcnew System::Windows::Forms::TextBox());
			this->buttonTest = (gcnew System::Windows::Forms::Button());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// buttonStart
			// 
			this->buttonStart->Location = System::Drawing::Point(13, 13);
			this->buttonStart->Name = L"buttonStart";
			this->buttonStart->Size = System::Drawing::Size(75, 23);
			this->buttonStart->TabIndex = 0;
			this->buttonStart->Text = L"���";
			this->buttonStart->UseVisualStyleBackColor = true;
			this->buttonStart->Click += gcnew System::EventHandler(this, &MainForm::start_Click);
			// 
			// panel1
			// 
			this->panel1->BackColor = System::Drawing::SystemColors::ControlLight;
			this->panel1->Location = System::Drawing::Point(13, 43);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(601, 301);
			this->panel1->TabIndex = 2;
			this->panel1->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &MainForm::panel1_Paint);
			this->panel1->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MainForm::mouseAim);
			// 
			// buttonLoad
			// 
			this->buttonLoad->Location = System::Drawing::Point(231, 13);
			this->buttonLoad->Name = L"buttonLoad";
			this->buttonLoad->Size = System::Drawing::Size(75, 23);
			this->buttonLoad->TabIndex = 3;
			this->buttonLoad->Text = L"���������";
			this->buttonLoad->UseVisualStyleBackColor = true;
			this->buttonLoad->Click += gcnew System::EventHandler(this, &MainForm::loadClick);
			this->buttonLoad->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MainForm::mouseAim);
			// 
			// textBoxPath
			// 
			this->textBoxPath->Location = System::Drawing::Point(312, 14);
			this->textBoxPath->Name = L"textBoxPath";
			this->textBoxPath->Size = System::Drawing::Size(161, 20);
			this->textBoxPath->TabIndex = 4;
			// 
			// buttonTest
			// 
			this->buttonTest->Location = System::Drawing::Point(118, 13);
			this->buttonTest->Name = L"buttonTest";
			this->buttonTest->Size = System::Drawing::Size(75, 23);
			this->buttonTest->TabIndex = 5;
			this->buttonTest->Text = L"����";
			this->buttonTest->UseVisualStyleBackColor = true;
			this->buttonTest->Click += gcnew System::EventHandler(this, &MainForm::testClick);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(583, 14);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 6;
			this->button1->Text = L"���������";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MainForm::saveClick);
			// 
			// MainForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(735, 389);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->buttonTest);
			this->Controls->Add(this->textBoxPath);
			this->Controls->Add(this->buttonLoad);
			this->Controls->Add(this->panel1);
			this->Controls->Add(this->buttonStart);
			this->Name = L"MainForm";
			this->Text = L"MainForm";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion


	private: 
		System::Void start_Click(System::Object^  sender, System::EventArgs^  e)
		{
			if (x >= 0 && y >= 0)
				G->play(x, y);
			else
				G->play();
			panel1->Invalidate();
			x = -1;
			y = -1;
		}

	private:
		System::Void testClick(System::Object^  sender, System::EventArgs^  e)
		{
			TestGame();
			panel1->Invalidate();
		}

	private:
		void show(Graphics^ g, Cell* c)
		{
			if (typeid(*c) == typeid(Violator))
			{
				g->DrawImage(Image::FromFile("Violator.png"), c->getX() * 20, c->getY() * 20, 20, 20);
			}
			else if (typeid(*c) == typeid(Robot))
			{
				g->DrawImage(Image::FromFile("Robot.png"), c->getX() * 20, c->getY() * 20, 20, 20);
			}
			else if (typeid(*c) == typeid(Platform))
			{
				g->DrawImage(Image::FromFile("Platform.png"), c->getX() * 20, c->getY() * 20, 20, 20);
			}
			else if (typeid(*c) == typeid(Platform))
			{

			}
		}
	private: 
		System::Void panel1_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) 
		{
			list <Cell*> ls = G->getAll();
			Graphics^ g = e->Graphics;
			for (int i = 0; i <= G->getFieldWidth(); i++)
				g->DrawRectangle(gcnew Pen(Color::Azure), i * 20, 0, 1, 20 * G->getFieldHeight());
			for (int i = 0; i <= G->getFieldHeight(); i++)
				g->DrawRectangle(gcnew Pen(Color::Azure), 0, i * 20, 20 * G->getFieldWidth(), 1);
			g->DrawRectangle(gcnew Pen(Color::Purple, 2), 20 * G->getFieldWidth() - 20, 0, 20, 20 * G->getFieldHeight());
			for each(Cell* c in ls)
				show(g,c);
			for each(Cell* c in ls)
			{
				if (typeid(*c) == typeid(Violator))
				{
					g->DrawRectangle(gcnew Pen(Color::Red, 2), (int)c->getX() * 20, (int)c->getY() * 20, 20, 20);
					if (c->getX() == 29)
					{
						MessageBox::Show("Congatuation!");
						((Violator*)c)->dead();
					}
				}
				else
				{
					if (typeid(*c) == typeid(Robot))
					{
						g->DrawRectangle(gcnew Pen(Color::Blue, 2), (int)c->getX() * 20, (int)c->getY() * 20, 20, 20);
						for each (Module* m in ((Robot*)c)->getModules())
						{
							if (typeid(*m) == (typeid(Arms)))
							{
								//g->DrawArc(gcnew Pen(Color::DarkOliveGreen, 1), c->getX() * 20 + 3, c->getY() * 20 + 3, 5, 5, 0, 360);
								g->DrawRectangle(gcnew Pen(Color::DarkCyan, 1), ((int)c->getX() - m->getRadius()) * 20, ((int)c->getY() - m->getRadius()) * 20, (int)(20 * (m->getRadius() * 2 + 1)), (int) (20 * (m->getRadius() * 2 + 1)));
							}
						}
					}
					else
					{
						if (typeid(*c) == typeid(Platform))
						{
							g->DrawRectangle(gcnew Pen(Color::DarkGreen, 2), (int)c->getX() * 20, (int)c->getY() * 20, 20, 20);
							for each (Module* m in ((Platform*)c)->getModules())
							{
								if (typeid(*m) == (typeid(Ray)) || typeid(*m) == (typeid(Optic)))
									;//g->DrawRectangle(gcnew Pen(Color::DarkOliveGreen, 1), c->getX() * 20 + 3, c->getY() * 20 + 3, 5, 5);
								else
								{
									//g->DrawEllipse(gcnew Pen(Color::DarkOliveGreen, 1), c->getX() * 20 + 10, c->getY() * 20 + 3, 5, 7);
									g->DrawRectangle(gcnew Pen(Color::Brown, 1), ((int)c->getX() - m->getRadius()) * 20, ((int)c->getY() - m->getRadius()) * 20, (int)(20 * (m->getRadius() * 2 + 1)), (int)(20 * (m->getRadius() * 2 + 1)));
								}
							}
						}
						else
							g->DrawRectangle(gcnew Pen(Color::Black, 2), (int)c->getX() * 20, (int)c->getY() * 20, 20, 20);
					}
				}
			}
		}


	private: 
		void TestGame()
		{
			G = new Game(30, 15);
			G->addViolator(1, 0, 2);
			G->addViolator(1, 7, 2);
			G->addViolator(1, 14, 2);
			G->addPlatform(0, 7, 5);
			G->addSensor(0, 7, 5, 1, 0, 1, true);
			for (int i = 1; i < 6; i++)
			{
				G->addPlatform(5 * i, 2, 10);
				G->addNet(5 * i, 2, 3, 1, 0, 1, 5);
				G->addSensor(5 * i, 2, 5, 1, 0, 1, true);
				G->addRobot(5 * i + 1, 2, 5, 1);
				G->addArms(5 * i + 1, 2, 1, 1, 0, 1, 1);
			}
			for (int i = 1; i < 6; i++)
			{
				G->addPlatform(5 * i, 13, 10);
				G->addNet(5 * i, 13, 3, 1, 0, 1, 5);
				G->addSensor(5 * i, 13, 5, 1, 0, 1, true);
				G->addRobot(5 * i + 1, 12, 5, 1);
				G->addArms(5 * i + 1, 12, 1, 1, 0, 1, 1);
			}
		}
	private: 
		System::Void saveClick(System::Object^  sender, System::EventArgs^  e) 
		{
			StreamWriter^ S;
			try 
			{
				S = File::CreateText(textBoxPath->Text);
			}
			catch (NotSupportedException^ )
			{
				MessageBox::Show("Incorrect path");
				return;
			}
			S->WriteLine(G->getFieldWidth());
			S->WriteLine(G->getFieldHeight());
			list <Cell*> ls = G->getAll();
			for each (Cell* c in ls)
			{
				S->WriteLine("Object");
				if (typeid(*c) == typeid(Cell))
					S->WriteLine("Bar");
				else if (typeid(*c) == typeid(Robot))
					S->WriteLine("Robot");
				else if (typeid(*c) == typeid(Platform))
					S->WriteLine("Platform");
				else if (typeid(*c) == typeid(Violator))
					S->WriteLine("Violator");
				S->WriteLine(c->getX());
				S->WriteLine(c->getY());
				if (typeid(*c) == typeid(Robot))
				{
					S->WriteLine(((Robot*)c)->getPower());
					S->WriteLine(((Robot*)c)->getSpeed());
				}
				else if (typeid(*c) == typeid(Platform))
					S->WriteLine(((Platform*)c)->getPower());
				else if (typeid(*c) == typeid(Violator))
					S->WriteLine(((Violator*)c)->getSpeed());
				if (typeid(*c) == typeid(Robot) || typeid(*c) == typeid(Platform))
				{
					vector <Module* > modules = ((Platform*)c)->getModules();
					for each (Module* m in modules)
					{
						S->WriteLine("Module");
						if (typeid(*m) == typeid(Arms))
							S->WriteLine("Arms");
						else if (typeid(*m) == typeid(Optic))
							S->WriteLine("Optic");
						else if (typeid(*m) == typeid(Ray))
							S->WriteLine("Ray");
						else
							S->WriteLine("Net");
						S->WriteLine(c->getX());
						S->WriteLine(c->getY());
						S->WriteLine(m->getRadius());
						S->WriteLine(m->getLevelHigh());
						S->WriteLine(m->getLevelLow());
						S->WriteLine(m->getSize());
						if (typeid(*c) == typeid(Arms))
							S->WriteLine(((Arms*)c)->getTime());
					}
				}
			}
			S->Close();
		}

	private:
		System::Void loadClick(System::Object^  sender, System::EventArgs^  e)
		{
			int i = 0;
			if (File::Exists(textBoxPath->Text))
			{

				StreamReader^ S = File::OpenText(textBoxPath->Text);
				try
				{
					int width = Convert::ToInt32(S->ReadLine());
					i++;
					int height = Convert::ToInt32(S->ReadLine());
					i++;
					delete G;
					G = new Game(width, height);
					panel1->Width = width * 20 + 1;
					panel1->Height = height * 20 + 1;
				}
				catch (FormatException^)
				{
					MessageBox::Show("Incorrect size");
				}
				while (!S->EndOfStream)
				{
					try
					{
						String^ type = S->ReadLine(); i++;
						i++;
						if (type == "Object")
						{
							type = S->ReadLine();
							unsigned int x = Convert::ToUInt32(S->ReadLine());
							i++;
							unsigned int y = Convert::ToUInt32(S->ReadLine()); i++;
							if (type == "Bar")
								G->addBar(x, y);
							else if (type == "Violator")
							{
								unsigned int speed = Convert::ToUInt32(S->ReadLine()); i++;
								G->addViolator(x, y, speed);
							}
							else if (type == "Robot")
							{
								unsigned int power = Convert::ToUInt32(S->ReadLine()); i++;
								unsigned int speed = Convert::ToUInt32(S->ReadLine()); i++;
								G->addRobot(x, y, power, speed);
							}
							else if (type == "Platform")
							{
								unsigned int power = Convert::ToUInt32(S->ReadLine()); i++;
								G->addPlatform(x, y, power);
							}
						}
						else
						{
							type = S->ReadLine();
							unsigned int x = Convert::ToUInt32(S->ReadLine()); i++;
							unsigned int y = Convert::ToUInt32(S->ReadLine()); i++;
							unsigned int radius = Convert::ToInt32(S->ReadLine()); i++;
							unsigned int levelHigh = Convert::ToUInt32(S->ReadLine()); i++;
							unsigned int levelLow = Convert::ToUInt32(S->ReadLine()); i++;
							unsigned int size = Convert::ToUInt32(S->ReadLine()); i++;
							if (type == "Ray")
								G->addSensor(x, y, radius, levelHigh, levelLow, size, true);
							else if (type == "Optic")
								G->addSensor(x, y, radius, levelHigh, levelLow, size, false);
							else if (type == "Arms")
							{
								G->addArms(x, y, radius, levelHigh, levelLow, size, 5);
							}
							else if (type == "Net")
								G->addNet(x, y, radius, levelHigh, levelHigh, size, 10);
						}
					}
					catch (FormatException^)
					{
						String^ S = gcnew String("Incorrect arguments, line: ");
						S += i;
						MessageBox::Show(S);
						return;
					}
					catch (Exception^ e)
					{
						MessageBox::Show(e->ToString());
						return;
					}
				}
				S->Close();
			}
			else
				MessageBox::Show("Incorrect path!");
			panel1->Invalidate();
		}
	private: int x;
	private: int y;

	private: 
		System::Void mouseAim(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) 
		{
			x = e->X / 20;
			y = e->Y / 20;
			String^ s = "";
			s += x;
			s += "  ";
			s += y;

			//panel1->Invalidate();
			//panel1->Graphics->DrawRectangle(gcnew Pen(Color::Black), x * 20, y * 20, 1, 20 * G->getFieldHeight());


		}
	};
}
