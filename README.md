**What is this repository for?**

This repository contains the solution for the task "Protection system", variant 28. It describes protection system which contains her components. There are components(pltforms), mobile components(robots, mobile platforms), enviroment, art itellect, violators. Components contains modules which can be NetModule, Sensor, Arming. AI should renders violators.

**How do I get set up?**

This repository contains two solutions, created in VS2015 using C++. To open application with graphic application(final realisation) you should have VS2015 installed and run 3_3_28_15_Volkov_121.sln(in branch "master"). If you want to run graphic application, you should Set as StartUp project 3_3_28_15_Volkov_121_gui in properties. To open application with tests and console application(dialog) application you should have VS2015 installed and run 3_3_28_15_Volkov_121_t.sln(in branch "master1"). If you want to run tests, you should have GoogleTest library included and Set 3_3_28_15_Volkov_121_t_test as StartUp project in properties. If you want to run dialog program, you should Set as StartUp project 3_3_28_15_Volkov_121_t_app in properties.To open application with my template and it's tests you should have VS2015 installed and run STL.sln(in branch "master2"). If you want to run tests, you should have GoogleTest library included and Set STL_test as StartUp project in properties. If you want to run console program, you should Set as StartUp project STL_app in properties.The documentation for this programm you can see in "Downloads".

**Who do I talk to?**

Volkov Evgeny, group K03-121, NRNU MEPhI.